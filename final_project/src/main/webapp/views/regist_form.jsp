<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Sign Up</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/formRegist.css"/>">
<!-- <script src="vendor/jquery/jquery.min.js"></script> -->
<!-- <script src="js/main.js"></script> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<!-- Form dang nhap -->
		<h2 style="text-align: center;">Welcome to TATALO!</h2>
		<form action="./login" method="POST">
			<div class="row">
				<div class="col col-md-6">
					<a href="#" class="fb btn"><i class="fab fa-facebook"></i>
						Login with Facebook </a> <a href="#" class="twitter btn"><i
						class="fab fa-twitter-square"></i>Login with Twitter </a> <a href="#"
						class="google btn"><i class="fab fa-google-plus-square"></i>
						Login with Google+ </a>
				</div>
				<div class="col col col-md-6">
					<div class="hide-md-lg">
						<p>Or sign in manually:</p>
					</div>
					<input type="text" name="email" placeholder="Email" required>
					<input type="password" name="password" placeholder="Password"
						required> <input type="submit" value="Login">
					<ul class="other" style="list-style: none">
						<li><a href="">Forgot password?</a></li>
						<li><a data-toggle="modal" data-target="#myModal" href="">Sign
								up</a></li>
					</ul>
				</div>
			</div>
		</form>
		<!-- Form dang ky -->


		<!-- The Modal -->
		<div class="modal" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Registration</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">
						<form action="./regist" method="POST">
							<div class="container">
								<h1>Register</h1>
								<p>Please fill in this form to create an account.</p>
								<hr>

								<label for="username"><b>Name</b></label> <input type="text"
									placeholder="Enter username" name="username" required> <label
									for="phone"><b>Phone</b></label> <input type="text"
									placeholder="Enter phone" name="phone" required> <label
									for="email"><b>Email</b></label> <input type="text"
									placeholder="Enter Email" name="email" required> <label
									for="psw"><b>Password</b></label> <input type="password"
									placeholder="Enter Password" name="psw" required> <label
									for="psw-repeat"><b>Repeat Password</b></label> <input
									type="password" placeholder="Repeat Password" name="psw-repeat"
									required>
								<hr>

								<p>
									By creating an account you agree to our <a href="#">Terms &
										Privacy</a>.
								</p>
								<button type="submit"
									class="registerbtn btn btn-outline-secondary"
									style="width: 100px;">Register</button>
								
							</div>

							<div class="container signin">
								<p>
									Already have an account? <a href="" data-toggle="modal"
										data-target="#myModal">Sign in</a>.
								</p>
							</div>
						</form>
					</div>
					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>

	</div>


</body>
</html>