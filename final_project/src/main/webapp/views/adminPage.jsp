<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Admin Page</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/style.css"/>">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-sm">
			<a class="navbar-brand" href="#">TATALO</a>
			<button class="navbar-toggler d-lg-none" type="button"
				data-toggle="collapse" data-target="#collapsibleNavId"
				aria-controls="collapsibleNavId" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="collapsibleNavId">
				<ul class="navbar-nav ">
					<li class="nav-item active mr-2"><i class="fa fa-tasks"></i></li>
					<li class="nav-item mr-2"><i class="fa fa-envelope-o"></i></li>
					<li class="nav-item mr-2"><i class="fa fa-bell-o"></i></li>
				</ul>
			</div>
			<div class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="dropdownId"
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
				<div class="dropdown-menu" aria-labelledby="dropdownId">
					<a class="dropdown-item" data-toggle="modal"
						data-target="#statementModal">Statement</a> <a
						class="dropdown-item" href="#">Exit</a>
				</div>
			</div>
			</div>
		</nav>
	</header>
	<div class="d-flex justify content-between">
		<div id="side-bar">
			<div class="logo">ADMIN PAGE</div>
			<ul class="list-group rounded-0">
				<li class="dashboard">DASHBOARD</li>
				<li><a href="./quanlyDanhMuc.html"> <i
						class="fa fa-book mr-2"></i> Catalog management
				</a></li>
				<li><a href="#"> <i class="fa fa-user mr-2"></i> User
						management
				</a></li>
				<li><a href="./QuanlySanPham.html"><i
						class="fa fa-cart-plus"></i> Product management </a></li>
				<li><a href="#"><i class="fa fa-tags"></i> Customer message
				</a></li>
				<li><a href="#"><i class="fa fa-star"></i> Show the
						gathered points </a></li>

			</ul>
		</div>

		<!--Forms-->

		<!-- The Modal -->
		<div class="modal" id="statementModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">STATEMENT</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">
						<form
							style="display: flex; flex-direction: row; justify-content: space-around;">


							<div
								style="display: flex; flex-direction: column; justify-content: space-around; width: 300px;">
								<legend>Admin information:</legend>
								<div
									style="display: flex; flex-direction: row; justify-content: space-between;">
									<label for="nameField">Name</label> <input type="text"
										id="nameField" style="width: 170px;">
								</div>
								<div
									style="display: flex; flex-direction: row; justify-content: space-between;">
									<label for="ageField">Email</label> <input type="text"
										id="emailField" style="width: 170px;">
								</div>
								<div
									style="display: flex; flex-direction: row; justify-content: space-between;">
									<label for="addressField">Address</label> <input type="text"
										id="addressField" style="width: 170px;">
								</div>
								<div
									style="display: flex; flex-direction: row; justify-content: space-between;">
									<label for="positionField">Phone</label> <input type="text"
										id="positionField" style="width: 170px;">
								</div>
								<input class="button-primary" type="submit" value="Send">

							</div>

						</form>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>

<!-- 
	<!-- wizard -->
		<div class="row-fluid section">
			<!-- block -->
			<div class="block">
				<div class="navbar navbar-inner block-header">
					<div class="muted pull-left">Form Wizard</div>
				</div>
				<div class="block-content collapse in">
					<div class="span12">
						<div id="rootwizard">
							<div class="navbar">
								<div class="navbar-inner">
									<div class="container">
										<ul>
											<li><a href="#tab1" data-toggle="tab">Step 1</a></li>
											<li><a href="#tab2" data-toggle="tab">Step 2</a></li>
											<li><a href="#tab3" data-toggle="tab">Step 3</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div id="bar" class="progress progress-striped active">
								<div class="bar"></div>
							</div>
							<div class="tab-content">
								<div class="tab-pane" id="tab1">
									<form class="form-horizontal">
										<fieldset>
											<div class="control-group">
												<label class="control-label" for="focusedInput">Name</label>
												<div class="controls">
													<input class="input-xlarge focused" id="focusedInput"
														type="text" value="">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="focusedInput">Email</label>
												<div class="controls">
													<input class="input-xlarge focused" id="focusedInput"
														type="text" value="">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="focusedInput">Phone</label>
												<div class="controls">
													<input class="input-xlarge focused" id="focusedInput"
														type="text" value="">
												</div>
											</div>
										</fieldset>
									</form>
								</div>
								<div class="tab-pane" id="tab2">
									<form class="form-horizontal">
										<fieldset>
											<div class="control-group">
												<label class="control-label" for="focusedInput">Address</label>
												<div class="controls">
													<input class="input-xlarge focused" id="focusedInput"
														type="text" value="">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="focusedInput">City</label>
												<div class="controls">
													<input class="input-xlarge focused" id="focusedInput"
														type="text" value="">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="focusedInput">State</label>
												<div class="controls">
													<input class="input-xlarge focused" id="focusedInput"
														type="text" value="">
												</div>
											</div>
										</fieldset>
									</form>
								</div>
								<div class="tab-pane" id="tab3">
									<form class="form-horizontal">
										<fieldset>
											<div class="control-group">
												<label class="control-label" for="focusedInput">Company
													Name</label>
												<div class="controls">
													<input class="input-xlarge focused" id="focusedInput"
														type="text" value="">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="focusedInput">Contact
													Name</label>
												<div class="controls">
													<input class="input-xlarge focused" id="focusedInput"
														type="text" value="">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="focusedInput">Contact
													Phone</label>
												<div class="controls">
													<input class="input-xlarge focused" id="focusedInput"
														type="text" value="">
												</div>
											</div>
										</fieldset>
									</form>
								</div>
								<ul class="pager wizard">
									<li class="previous first" style="display: none;"><a
										href="javascript:void(0);">First</a></li>
									<li class="previous"><a href="javascript:void(0);">Previous</a></li>
									<li class="next last" style="display: none;"><a
										href="javascript:void(0);">Last</a></li>
									<li class="next"><a href="javascript:void(0);">Next</a></li>
									<li class="next finish" style="display: none;"><a
										href="javascript:;">Finish</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /block -->
		</div>
		<!-- /wizard -->
 -->
		
</body>
</html>