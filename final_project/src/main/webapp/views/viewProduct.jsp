<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/viewProduct.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/css/viewProduct.css" />">


<style type="text/css">
#table-more-detail th {
	background-color: #EFEFEF;
	width: 200px;
	border: 1px solid #e6e6e6;
}

#table-more-detail th, td {
	padding-left: 10px;
}

#table-more-detail tr {
	border: 1px solid #e6e6e6;
}

#table_question-answer p {
	font-size: 14px;
}

.mySlides {
	display: none;
}
</style>
<title>View Product</title>
</head>
<body style="background-color: #F4F4F4;"
	onload="changeStarColorViewProduct(${product.pro_star});separatePageViewed(${fn:length(listViewed)})">
	<div id="header">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="#"><i class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link" href="#"><i
										class="fa fa-fw fa-home"></i>Home</a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i>Category</a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<a class="dropdown-item" href="#">Technology</a> <a
												class="dropdown-item" href="#">Fashion</a> <a
												class="dropdown-item" href="#">Food and Drink</a>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link" href="#"><i
										class="fa fa-fw fa-envelope"></i>Contact us</a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i>Languages</a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">Sign
										in/Sign up</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link" href="#">VN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">EN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">LA</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">
					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>

			</div>

			<c:if test="${currentUser == null}">
				<button onclick="location.href='./movetologin';" class="btn"
					id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span>Log in</span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${currentUser.user_name}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">My order list</a> <a
							class="dropdown-item" href="#">Account detail</a>
						<c:if test="${role == 'admin'}">
							<a class="dropdown-item" href="../categoryController/categories">Admin
								page</a>
						</c:if>
						<a class="dropdown-item" href="../loginRegisterController/logout">Log
							out</a>
					</div>
				</div>

			</c:if>

			<c:if test="${currentUser != null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="goInToCart()"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart">${cartItems}</span></i>
				</button>
			</c:if>
			<c:if test="${currentUser == null}">
				<button class="btn" id="btnCart" type="button" value="${user_id}"
					onclick="goInToCart()"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
					<i class="fas fa-shopping-cart"><span class="badge badge-light"
						style="background-color: yellow; margin-left: 5px;"
						id="num-item-in-cart">0</span></i>
				</button>
			</c:if>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>

	<div id="content" class="container"
		style="display: flex; flex-direction: column; margin-top: 100px;">
		<div id="first-row"
			style="height: 400px; display: flex; flex-direction: row;">
			<div id="first-row-left" class=""
				style="width: 500px; display: flex; flex-direction: row; border-right: 1px solid #e6e6e6;">
				<div id="big-image-container"
					style="background-color: white; width: 499px; display: flex; flex-direction: column; justify-content: space-between; border-right: 1px solid #e5e5e5;">
					<div
						style="height: 371px; width: 500px; display: flex; flex-direction: row; justify-content: space-around;">
						<c:forEach items="${listImage}" var="image">
							<img class="mySlides" src="<c:url value="${image.img}"/>"
								style="width: 400px; height: 350px">
						</c:forEach>
					</div>

					<div
						style="display: flex; flex-direction: row; justify-content: space-between;">
						<button class="btn btn-secondary w3-display-left"
							onclick="plusDivs(-1)">&#10094;</button>
						<button class="btn btn-secondary w3-display-right"
							onclick="plusDivs(+1)">&#10095;</button>
					</div>

					<script type="text/javascript">
						var myIndex = 0;
						carousel();

						function carousel() {
						  var i;
						  var x = document.getElementsByClassName("mySlides");
						  for (i = 0; i < x.length; i++) {
						    x[i].style.display = "none";  
						  }
						  myIndex++;
						  if (myIndex > x.length) {myIndex = 1}    
						  x[myIndex-1].style.display = "block";  
						  setTimeout(carousel, 2000); // Change image every 2 seconds
						}
					</script>
				</div>
			</div>

			<div id="first-row-right"
				style="background-color: white; width: 609px; display: flex; flex-direction: column;">
				<div id="item-title">
					<p style="margin-bottom: 5px; padding-left: 20px; font-size: 30px;">
						${product.pro_name}</p>
					<table id="table-star" style="margin-left: 10px;">
						<tr>
							<td><i class="far fa-star"></i></td>
							<td><i class="far fa-star"></i></td>
							<td><i class="far fa-star"></i></td>
							<td><i class="far fa-star"></i></td>
							<td><i class="far fa-star"></i></td>
							<td></td>
						</tr>
					</table>
					<hr>
				</div>
				<div id="item-info" style="padding-left: 20px;">
					<p id="price">
						<!-- 150000 -->${product.pro_cost}&#8363</p>
				</div>
				<div id="item-buy"
					style="display: flex; flex-direction: row; padding-left: 20px;">
					<div id="quantity"
						style="display: flex; flex-direction: column; margin-right: 5px;">
						<p style="font-size: 14px; margin-bottom: 2px;">Quantity</p>
						<div id="quantity-selector"
							style="display: flex; flex-direction: row;">
							<button id="btn-descrease" type="button"
								class="btn btn-outline-secondary">
								<i class="fas fa-chevron-down"></i>
							</button>
							<input value="1" id="quantity-field"
								style="width: 40px; height: 38px; text-align: center;">
							<button id="btn-inscrease" type="button"
								class="btn btn-outline-secondary">
								<i class="fas fa-chevron-up"></i>
							</button>
						</div>
					</div>
					<c:if test="${currentUser != null}">
						<div id="buy-button-container"
							style="justify-content: space-around; display: flex; width: 100px;">
							<button id="btnBuy" class="btn btn-danger" type="button"
								style="width: 100px;" onclick="addCart(${product.pro_id}, 0)">
								<i class="fas fa-shopping-cart"></i>Add to cart
							</button>
						</div>
					</c:if>
					<c:if test="${currentUser == null}">
						<div id="buy-button-container"
							style="justify-content: space-around; display: flex; width: 100px;">
							<button id="btnBuy" class="btn btn-danger" type="button"
								style="width: 100px;" onclick="addCart(${product.pro_id}, 1)">
								<i class="fas fa-shopping-cart"></i>Add to cart
							</button>
						</div>
					</c:if>
				</div>
			</div>
		</div>
		<h4 style="margin-top: auto; margin-bottom: auto;">MORE DETAIL</h4>
		<div id="second-row"
			style="height: 300px; justify-content: space-around; display: flex; flex-direction: row; background-color: white;">
			<div
				style="display: flex; flex-direction: column; justify-content: space-around;">
				<table id="table-more-detail"
					style="width: 1060px; height: 250px; border: 1px solid black;"
					class="">
					<tr>
						<th>Brand</th>
						<td>Home Made</td>
					</tr>
					<tr>
						<th>Screen size</th>
						<td>6.5 inch</td>
					</tr>
					<tr>
						<th>Rear camera</th>
						<td>13MP + 8MP</td>
					</tr>
					<tr>
						<th>Front camera</th>
						<td>13MP</td>
					</tr>
					<tr>
						<th>Ram memory</th>
						<td>8GB</td>
					</tr>
				</table>
			</div>
		</div>
		<c:if test="${fn:length(listViewed) != 0 }">
			<h4 style="margin-top: auto; margin-bottom: auto;">VIEWED ITEMS</h4>
			<div id="third-row" style="height: 400px;">
				<c:set var="count" value="0"></c:set>
				<div id="viewed-items-container"
					style="height: 350px; position: relative;">
					<div id="arrows"
						style="z-index: 1; height: 36px; width: 1110px; margin: 0; position: absolute; top: 50%; -ms-transform: translateY(-50%); transform: translateY(-50%);">
						<div
							style="display: flex; flex-direction: row; justify-content: space-between; width: 1110px;">
							<button class="btn btn-outline-secondary" style="border: none;"
								id="btnPre" var="pre" value="${count}">-</button>
							<button class="btn btn-outline-secondary" style="border: none;"
								id="btnNext" value="${count+4}">+</button>
						</div>
					</div>

					<table id="table-viewed-item"
						style="height: 350px; padding-left: 0px;">
						<c:set var="start" value="0"></c:set>
						<c:forEach end="${fn:length(listViewed)/5-1}" var="row" begin="1">
							<c:if test="${row==1}">
								<tr style="display: block;">
									<c:forEach items="${listViewed}" var="item" begin="${start}"
										end="${start+4}">
										<td style="padding-left: 0px;">
											<div class="card"
												style="width: 222px; height: 350px; text-align: center;">
												<img id="pro_img" class="card-img-top"
													src="<c:url value="/resources/image/findx.jpg"/>"
													alt="Card image"
													style="width: 120px; height: 180px; position: relative; margin: auto;">
												<div class="card-body" style="padding: 0.5rem;">
													<h5 class="card-title" id="pro_name">${item.pro_name}</h5>
													<p class="card-text" id="pro_description">${fn:substring(item.pro_content, 0, 20)}</p>
													<p class="card-text" id="pro_price">
														<strong>${item.pro_cost} &#8363</strong>
													</p>
												</div>
											</div>
										</td>
									</c:forEach>
									<c:set var="start" value="${start+5}"></c:set>
								</tr>
							</c:if>
							<tr style="display: none;">
								<c:forEach items="${listViewed}" var="item" begin="${start}"
									end="${start+4}">
									<td style="padding-left: 0px;">
										<div class="card"
											style="width: 222px; height: 350px; text-align: center;">
											<img id="pro_img" class="card-img-top"
												src="<c:url value="/resources/image/findx.jpg"/>"
												alt="Card image"
												style="width: 120px; height: 180px; position: relative; margin: auto;">
											<c:set var="t" value="getStartNum();"></c:set>
											<div class="card-body" style="padding: 0.5rem;">
												<h5 class="card-title" id="pro_name">${item.pro_name}</h5>
												<p class="card-text" id="pro_description">${fn:substring(item.pro_content, 0, 20)}</p>
												<p class="card-text" id="pro_price">
													<strong>${item.pro_cost} &#8363</strong>
												</p>
											</div>
										</div>
									</td>
								</c:forEach>
								<c:set var="start" value="${start+5}"></c:set>
							</tr>
						</c:forEach>
						<c:set var="start" value="${start}"></c:set>
						<c:if test="${fn:length(listViewed)%5 != 0}">
							<c:forEach end="${fn:length(listViewed)%5}" var="row" begin="0">
								<tr style="display: none;">
									<c:forEach items="${listViewed}" var="item" begin="${start}"
										end="${start+4}">
										<td style="padding-left: 0px;">
											<div class="card"
												style="width: 222px; height: 350px; text-align: center;">
												<img id="pro_img" class="card-img-top"
													src="<c:url value="/resources/image/findx.jpg"/>"
													alt="Card image"
													style="width: 120px; height: 180px; position: relative; margin: auto;">
												<c:set var="t" value="getStartNum();"></c:set>
												<div class="card-body" style="padding: 0.5rem;">
													<h5 class="card-title" id="pro_name">${item.pro_name}</h5>
													<p class="card-text" id="pro_description">${fn:substring(item.pro_content, 0, 20)}</p>
													<p class="card-text" id="pro_price">
														<strong>${item.pro_cost} &#8363</strong>
													</p>
												</div>
											</div>
										</td>
									</c:forEach>
									<c:set var="start" value="${start+5}"></c:set>
								</tr>
							</c:forEach>
						</c:if>
					</table>
					<div style="height: 100px;">
						<ul id="separate-links" class="pagination"
							style="display: flex; flex-direction: row; justify-content: center;">
							<!-- <li class="page-item"><button class="page-link" onclick="Comeback(this)" value = "0">Previous</button></li> -->
							<!-- <li class="page-item"><a class="page-link" onclick="">1</a></li>
						<li class="page-item"><a class="page-link" onclick="">2</a></li>
						<li class="page-item"><a class="page-link" onclick="">3</a></li> -->
							<!-- <li class="page-item"><a class="page-link" onclick="">Next</a></li> -->
						</ul>
					</div>
				</div>
			</div>
		</c:if>
		<h4 style="margin-top: auto; margin-bottom: auto;">QUESTION-ANSWER</h4>
		<div id="fourth-row" style="">
			<table id="table_question-answer"
				style="width: 1110px; background-color: white; border: 1px solid #e6e6e6; height: 500px;">
				<c:forEach items="${listQA}" var="qa">
					<tr style="border: 1px solid #e6e6e6">
						<td>
							<p style="font-weight: 700;" class="q-content">${qa.q_content}</p>
							<p class="a-content">${qa.a_content}</p>
						</td>
					</tr>
				</c:forEach>
				<tr style="border: 1px solid #e6e6e6; width: 42px;">
					<td><input type="text" id="txtQuestion"
						style="width: 850px; border-radius: .25rem; height: 40px; border: none;"
						placeholder="Your question here?">
						<button type="button" class="btn btn-warning" id="btnQuestion"
							style="width: 200px; height: 40px;"
							onclick="addQuestion(${product.pro_id})">Send</button></td>
				</tr>
			</table>
		</div>

		<h4 style="margin-top: auto; margin-bottom: auto;">REVIEW</h4>
		<div id="fifth-row"
			style="background-color: white; height: 800px; margin-bottom: 20px;">
			<div id="review-top"
				style="border-bottom: 1px solid #e6e6e6; height: 200px;">
				<div id="average-star"
					style="float: left; width: 50%; border-right: 1px solid #e6e6e6; height: 200px; display: inline-block; text-align: center;">
					<h3>Average</h3>
					<p style="color: red; font-size: 60px;">${product.pro_star}</p>
					<table id="table-star-review"
						style="margin-left: auto; margin-right: auto;">
						<tr>
							<td><i class="far fa-star"></i></td>
							<td><i class="far fa-star"></i></td>
							<td><i class="far fa-star"></i></td>
							<td><i class="far fa-star"></i></td>
							<td><i class="far fa-star"></i></td>
							<td></td>
						</tr>
					</table>
				</div>
				<div id="send-review"
					style="float: left; height: 200px; width: 50%; text-align: center; display: flex; flex-direction: column; justify-content: space-around;">
					<div id="before-press-review-button" style="">
						<p>You can review this product</p>
						<button id="open-review-field-button" class="btn btn-warning"
							style="width: 200px; margin-left: auto; margin-right: auto;">Review</button>
					</div>
					<div id="after-press-review-button"
						style="display: flex; flex-direction: column; display: none;">
						<div style="margin-left: 20px;">
							<p style="float: left; padding-left: 5px;">Review this
								product :</p>
							<table id="table-star-in-review"
								style="margin-left: 10px; float: left;">
								<tr>
									<td onmouseout="changeStarColorReviewOut(this)"
										onmousemove="changeStarColorReviewOver(this)"
										onclick="clickOnStars(this)"
										style="padding-left: 2px; padding-right: 2px;"><i
										class="far fa-star"></i></td>
									<td onmouseout="changeStarColorReviewOut(this)"
										onmousemove="changeStarColorReviewOver(this)"
										onclick="clickOnStars(this)"
										style="padding-left: 2px; padding-right: 2px;"><i
										class="far fa-star"></i></td>
									<td onmouseout="changeStarColorReviewOut(this)"
										onmousemove="changeStarColorReviewOver(this)"
										onclick="clickOnStars(this)"
										style="padding-left: 2px; padding-right: 2px;"><i
										class="far fa-star"></i></td>
									<td onmouseout="changeStarColorReviewOut(this)"
										onmousemove="changeStarColorReviewOver(this)"
										onclick="clickOnStars(this)"
										style="padding-left: 2px; padding-right: 2px;"><i
										class="far fa-star"></i></td>
									<td onmouseout="changeStarColorReviewOut(this)"
										onmousemove="changeStarColorReviewOver(this)"
										onclick="clickOnStars(this)"
										style="padding-left: 2px; padding-right: 2px;"><i
										class="far fa-star"></i></td>
								</tr>
							</table>
							<button onclick="removeStars()" id="btnRemoveStars" type="button"
								class="btn btn-danger" value=""
								style="height: 30px; width: 30px; border-radius: 30px; display: none;">-</button>
						</div>
						<div style="">
							<input type="text" id="write-review-textbox"
								placeholder="Write your review"
								style="border-radius: .25rem; width: 300px;">
							<button id="send-review-button" class="btn btn-warning"
								style="width: 200px;" name="send-review-button" value="3"
								onclick="addReview(${product.pro_id}, 2)">Review</button>
						</div>
						<button id="close-review-field-button" class="btn btn-warning"
							style="width: 80px; margin-left: auto; margin-right: auto; margin-top: 20px;">Close</button>
					</div>
				</div>
			</div>
			<div id="review-content">
				<table id="table-review"
					style="width: 1110px; height: 600px; text-align: center;">
					<c:forEach items="${listReview}" var="review">
						<tr style="border: 1px solid #e6e6e6">
							<c:set var="name"
								value="${fn:substring(review.account_detail.user_name, 0, 4)}"></c:set>
							<th
								style="width: 150px; border-right: 1px solid #e6e6e6; height: 100px;">
								<div
									style="width: 60px; height: 60px; border-radius: 60px; background-color: lavender; text-align: center; margin: auto; padding-top: 18px;">${name}</div>
								<p style="font-size: 11px;">${review.account_detail.user_name}</p>
							</th>
							<td>
								<p>${review.content}</p>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</div>

	<div class="container-fluid" id="footer-top">
		<div class="row" style="display: flex; flex-direction: column;">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links">
				<div class="links-in-footer">
					<p class="footer-title">INSTRUCTION</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">SERVICES</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">LINKED PAGE</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">PAYMENT</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">HOTLINE</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links-collapse">
				<div id="accordion">
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseOne">INSTRUCTION</p>
						<div id="collapseOne" class="collapse show"
							data-parent="#accordion">
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseTwo">SERVICES</p>
						<div id="collapseTwo" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse"
							href="#collapseThree">LINKED PAGES</p>
						<div id="collapseThree" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
				<div class="container" id="footer-bottom">
					<div class="footer-bottom-top">
						<div>
							<p>
								<i class="fa fa-store" style="font-size: 50px;"></i>
							</p>
							<br>
							<p style="margin-top: -40px; font-weight: 700;">Green Academy</p>
						</div>

						<div class="footer-bottom-top-right">
							<button type="button" id="btnFace">
								<i class="fab fa-facebook-f"></i>
							</button>
							<button type="button" id="btnIns">
								<i class="fab fa-instagram"></i>
							</button>
							<button type="button" id="btnYou">
								<i class="fab fa-youtube"></i>
							</button>
						</div>
					</div>
					<div class="footer-bottom-bottom">
						<div
							style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
							<div class="contact">
								<p style="margin-bottom: 2px;">Website:
									www.fashionstar.company</p>
								<p style="margin-bottom: 2px;">Äá»a chá»: 212A2 Nguyá»n
									TrÃ£i, P.Nguyá»n CÆ° Trinh, Q.1, Tp.Há» ChÃ­ Minh</p>
								<p style="margin-bottom: 2px;">[Email]:
									support@fashionstar.vn - [Hotline]: 1900 636 820 -[Tel]: (028)
									3925 5050 Ext 200 -[Fax]: (028) 3925 5050. Báº£n quyá»n Â©
									2015 cá»§a Fashion Star. Táº¥t cáº£ cÃ¡c quyá»n ÄÆ°á»£c báº£o
									lÆ°u</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- At here -->
</body>
</html>