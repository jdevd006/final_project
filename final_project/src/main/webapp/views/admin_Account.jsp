<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Account Management</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="<c:url value="/resources/css/admin_Account.css" />">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
</head>
<body>
	<header style="float: left;">
		<nav class="navbar navbar-expand-sm">
			<a class="navbar-brand" href="../homecontroller/homepage">TATALO</a>
			<button class="navbar-toggler d-lg-none" type="button"
				data-toggle="collapse" data-target="#collapsibleNavId"
				aria-controls="collapsibleNavId" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="collapsibleNavId">
				<ul class="navbar-nav ">
					<li class="nav-item active mr-2"><i class="fa fa-tasks"></i></li>
					<li class="nav-item mr-2"><i class="fa fa-envelope-o"></i></li>
					<li class="nav-item mr-2"><i class="fa fa-bell-o"></i></li>
				</ul>
			</div>
			<div class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="dropdownId"
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
				<div class="dropdown-menu" aria-labelledby="dropdownId">
					<a class="dropdown-item" data-toggle="modal"
						data-target="#statementModal">Statement</a> <a
						class="dropdown-item" href="#">Exit</a>
				</div>
			</div>
		</nav>
	</header>

	<div id="content" style="display: flex;">
		<div class="wrapper d-flex justify content-between">
			<div id="side-bar">
				<div class="logo">ADMIN PAGE</div>
				<ul class="list-group rounded-0">
					<li class="dashboard">DASHBOARD</li>
					<li><a href="../categoryController/categories"> <i
							class="fa fa-book mr-2"></i> Catalog management
					</a></li>
					<li><a href="../productController/products"> <i
							class="fa fa-user mr-2"></i> Product management
					</a></li>
					<li><a href="#"><i class="fa fa-cart-plus"></i> User
							management </a></li>
					<li><a href="#"><i class="fa fa-tags"></i> Customer
							message </a></li>
					<li><a href="#"><i class="fa fa-star"></i> Show the
							gathered points </a></li>
				</ul>
			</div>
		</div>

		<div style="margin-top: 60px;">
			<div id="wrapper-content" style="">
				<section class="control">
					<div class="row" style="margin-left: 1px; padding: 0 5px;">
						<div class="d-flex align-items-center ">
							<div class="input-group">
								<input type="text" class="form-control"
									placeholder="Name Catalog" id="searchName">
								<div class="input-group-prepend">
									<span class="input-group-text" id="btnTimNV"><i
										class="fa fa-search"></i></span>
								</div>
							</div>
						</div>
						<div class="align-items-center justify-content-end"
							style="display: flex; flex-direction: row; margin: auto; width: 400px;">
							<label style="width: 100px">Sort</label> <select id="mySeclect"
								style="width: 300px;">
								<option value="ten">Follow Name</option>
								<option value="id">Follow ID</option>
							</select>
							<form id="myRadioBtn" class="d-flex align-items-center m-2"
								action="">
								<input class="m-2" type="radio" name="gender" value="tang">Increase<br>
								<input class="m-2" type="radio" name="gender" value="giam">Decrease<br>
							</form>
						</div>
					</div>



					<button class="btn btn-primary" data-toggle="modal"
						data-target="#myModal" style="float: right;">Add</button>

					<!-- <div style="display: none;" id="test"> -->
					<h2>Add Account</h2>
					<div class="modal fade" id="myModal">
						<div class="modal-dialog">
							<form:form action="./user" method="post"
								modelAttribute="user">
								<div class="modal-content">

									<!-- Modal Header -->
									<div class="modal-header">
										<h4 class="modal-title">Add account</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>

									<!-- Modal body -->
									<div class="modal-body">
										<fieldset
											style="display: flex; flex-direction: column; justify-content: space-around;">
											<div class="control-group"
												style="display: flex; flex-direction: row; justify-content: space-between;">
												<label class="control-label" for="txtUsername">Full name</label>
												<form:input type="text" path="user_name" class="form-control"
													id="txtUsername" placeholder="Full name" style="width: 300px;"/>
											</div>
											<div class="control-group"
												style="display: flex; flex-direction: row; justify-content: space-between;">
												<label class="control-label" for="txtUseremail">Email</label>
												<form:input type="text" placeholder="Email"
													style="width: 300px;" path="email" id="txtUseremail" class="form-control" />
											</div>
											<div class="control-group"
												style="display: flex; flex-direction: row; justify-content: space-between;">
												<label class="control-label" for="txtUserphone">Phone number</label>
												<form:input type="text" placeholder="Phone"
													style="width: 300px;" path="phone" id="txtUserphone" class="form-control" />
											</div>
											<div class="control-group"
												style="display: flex; flex-direction: row; justify-content: space-between;">
												<label class="control-label" for="txtUserpassword">Password</label>
												<form:input type="password" placeholder="Password"
													style="width: 300px;" path="password" id="txtUserpassword" class="form-control"/>
											</div>
											<!-- <div class="form-group"
												style="display: flex; flex-direction: row; justify-content: space-between;">
												<label>Enter password again</label> <input id="password"
													type="password" name="" placeholder="Máº­t kháº©u"
													style="width: 300px;">
											</div>
											<div class="control-group">
												<label class="control-label" for="fileInput">Avatar</label>
												<div class="controls">
													<input class="input-file uniform_on" id="fileInput"
														type="file">
												</div>
											</div> -->
										</fieldset>
									</div>
									<!-- Modal footer -->
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary"
											name="btnSaveInModal" value="" id="btnSaveInModal">Save
											changes</button>
										<button type="button" class="btn btn-danger"
											data-dismiss="modal">Close</button>
									</div>
								</div>
							</form:form>
						</div>
					</div>
					<!-- </div> -->


					<section class="tableCatalog">
						<table class="table table-bordered" style="width: 900px;"
							id="products-table">
							<c:set var="count" value="1"></c:set>
							<c:set var="orderRole" value="0"></c:set>
							<thead>
								<tr>
									<th>Account_ID</th>
									<th>Account_Name</th>
									<th>Email</th>
									<th>Address</th>
									<th>Phone number</th>
									<th>Role</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${users}" var="user">
									<tr>
										<td>${user.user_id}</td>
										<td>${user.user_name}</td>
										<td>${user.email}</td>
										<td>${user.address.city}</td>
										<td>${user.phone}</td>
										<td>${roles[orderRole].role}</td>
										<td><button type="button" class="btn btn-primary"
												data-toggle="modal" data-target="#myModal"
												value="${product.pro_name}"
												onclick="update(this, ${product.pro_id})">Update</button>
											<button class="btn btn-danger" type="button"
												onclick="deleteProduct(this, ${product.pro_id})"
												value="${count}">Delete</button></td>
									</tr>
									<c:set var="count" value="${count+1}"></c:set>
									<c:set var="orderRole" value="${orderRole+1}"></c:set>
								</c:forEach>
							</tbody>
						</table>
					</section>
				</section>
			</div>
		</div>
	</div>
	<script>
	function deleteProduct(button, pro_id){
		console.log(pro_id);
		$.ajax({
			url : "./product/" + pro_id,
			type : "DELETE",
			data : "",
			contentType : "application/json",
			success : function(){
				var table = document.getElementById("products-table");
				table.deleteRow(button.value);
				//
				var tr = table.getElementsByTagName("tr");
				for (var i = 1; i < tr.length; i++){
					tr[i].getElementsByTagName("button")[1].value = i;
				}
			}
		});
	}
	function update(button, pro_id){
		$.ajax({
			url : "./aproduct?proId=" + pro_id,
			type : "GET",
			data : "",
			contentType : "application/json",
			success : function(data){
				document.getElementById("btnSaveInModal").value = "update-"+pro_id;
				
				document.getElementById("txtProductName").value = data[0].pro_name;
				document.getElementById("txtProductCost").value = data[0].pro_cost;
				document.getElementById("txtProductQuantity").value = data[0].pro_quantity;
				document.getElementById("txtProductContent").value = data[0].pro_content;
				var select = document.getElementById("category-select");
				/* for (var i = 0; i < select.getElementsByTagName("option").length; i++){
					console.log(select.getElementsByTagName("option")[i].id);
					} */
				document.getElementById("cate_id"+data[1].cate_id).selected = "true";
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
			})
		
		/* console.log(document.getElementById("btnSaveInModal").value);
		document.getElementById("txtProductName").value = button.value;
		document.getElementById("txtProductCost").value = product.pro_cost; */
	}
	</script>

</body>
</html>