<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/viewProduct.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/css/viewProduct.css" />">
</head>
<script type="text/javascript"
	src="<c:url value="/resources/js/homepagelistproduct.js"/>"></script>
<style>
.dropright {
	position: relative;
	display: inline-block;
}

.dropright-content {
	display: none;
	position: absolute;
	background-color: #edede8;
	min-width: 160px;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	z-index: 1;
}

.dropright-content a {
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
}

.dropright-content a:hover {
	background-color: #ddd;
}

.dropright:hover .dropright-content {
	display: block;
}

.dropright:hover .dropbtn {
	background-color: #95dee3;
}
</style>
<body onload="carouselSALE();carouselTECHNOLOGY()">
	<div id="header">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="../homecontroller/homepage"><i
						class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link"
									href="../homecontroller/homepage"><i
										class="fa fa-fw fa-home"></i>Home</a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i>Category</a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<a class="dropdown-item" href="#">Technology</a> <a
												class="dropdown-item" href="#">Fashion</a> <a
												class="dropdown-item" href="#">Food and Drink</a>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link" href="#"><i
										class="fa fa-fw fa-envelope"></i>Contact us</a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i>Languages</a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">Sign
										in/Sign up</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link" href="#">VN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">EN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">LA</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">
					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>

			</div>

			<c:if test="${currentUser == null}">
				<button
					onclick="location.href='../viewProductController/movetologin';"
					class="btn" id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span>Log in</span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${fn:substring(currentUser.user_name, 0, 5)}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item"
							href="/final_project/orderListController/orderList">My order
							list</a> <a class="dropdown-item" href="#">Account detail</a>
						<c:if test="${role == 'admin'}">
							<a class="dropdown-item" href="../categoryController/categories">Admin
								page</a>
						</c:if>
						<a class="dropdown-item" href="../loginRegisterController/logout">Log
							out</a>
					</div>
				</div>

			</c:if>

			<%-- <button class="btn" id="btnCart" type="button" value="${user_id}"
				onclick=""
				style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
				<i class="fas fa-shopping-cart"><span class="badge badge-light"
					style="background-color: yellow; margin-left: 5px;"
					id="num-item-in-cart">0</span></i>
			</button> --%>


			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>


	<div id="content"
		style="margin-top: 100px; background-image: /resources/image/Savin-NY-Website-Background-Web.jpg/>">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 br-blue">
					<div class="row">
						<i class="fa fa-fw fa-align-justify"></i>Category
					</div>
					<div class="row">
						<div class="dropright">
							<a href="#" class="text-info"> <span class="icon-wrap">
									<i class="fas fa-headphones"></i>
							</span> <span> Technology </span>
							</a>
							<div class="dropright-content">
								<div class="row">
									<a href="#">link1</a> <a href="#">link1</a> <a href="#">link1</a>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="dropright">
							<a href="#" class="text-info"> <span class="icon-wrap">
									<i class="fas fa-mask"></i>
							</span> <span> Fashion </span>
							</a>
							<div class="dropright-content">
								<div class="row">
									<a href="#">link1</a> <a href="#">link1</a> <a href="#">link1</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="dropright">
							<a href="#" class="text-info"> <span class="icon-wrap">
									<i class="fas fa-book"></i>
							</span> <span> Book </span>
							</a>
							<div class="dropright-content">
								<div class="row">
									<a href="#">link1</a> <a href="#">link1</a> <a href="#">link1</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="dropright">
							<a href="#" class="text-info"> <span class="icon-wrap">
									<i class="fas fa-home"></i>
							</span> <span> Home an Furniture </span>
							</a>
							<div class="dropright-content">
								<div class="row">
									<a href="#"></a> <a href="#">link1</a> <a href="#">link1</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-5">
					<div id="carousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ul class="carousel-indicators">
							<li data-target="#carousel" data-slide-to="0" class="active"></li>
							<li data-target="#carousel" data-slide-to="1"></li>
							<li data-target="#carousel" data-slide-to="2"></li>
						</ul>
						<!-- The slideshow -->
						<div class="carousel-inner">
							<div class="carousel-item active">
								<img id="pro_img" class="card-img-top"
									src="<c:url value="/resources/image/demo1.jpg"/>"
									alt="Carousel-image"
									style="width: 450; height: 230; position: relative; margin: auto;">
							</div>
							<div class="carousel-item">
								<img id="pro_img" class="card-img-top"
									src="<c:url value="/resources/image/demo2.jpg"/>"
									alt="Carousel-image"
									style="width: 450; height: 230; position: relative; margin: auto;">
							</div>
							<div class="carousel-item">
								<img id="pro_img" class="card-img-top"
									src="<c:url value="/resources/image/demo3.jpg"/>"
									alt="Carousel-image"
									style="width: 450; height: 230; position: relative; margin: auto;">
							</div>
						</div>
						<!-- Left and right controls -->
						<a class="carousel-control-prev" href="#carousel"
							data-slide="prev"> <span class="carousel-control-prev-icon"></span>
						</a> <a class="carousel-control-next" href="#carousel"
							data-slide="next"> <span class="carousel-control-next-icon"></span>
						</a>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card-img-fluid bg-dark"
						style="width: 350px; height: 275px">
						<c:if test="${currentUser== null }">
							<div class="card-header">
								<img class="rounded-circle rounded mx-auto d-block"
									src="<c:url value="/resources/image/img_avatar1.png"/>"
									alt="Card image" width="100" height="100">
							</div>

							<div class="text-center card-footer" style="margin-bottom: 0">
								<a href="../cartController/cartPage" class="btn btn-info"
									style="color: white"> See Cart </a>
							</div>

						</c:if>
						<c:if test="${currentUser != null }">
							<img class="rounded-circle rounded mx-auto d-block"
								src="<c:url value="/resources/image/img_avatar1.png"/>"
								alt="Card image" width="100" height="100">
							<div class="card-body ">
								<div class="text-center" style="margin-bottom: 0">
									<h4>${currentUser.user_name }</h4>
								</div>
								<div>
									<a href="#" class="btn btn-info" style="color: white">See
										Profile</a> <a href="../cartController/cartPage"
										class="btn btn-info" style="color: white">See Cart</a> <a
										href="#" class="btn btn-info" style="color: white">See
										History</a>

								</div>
							</div>
						</c:if>

					</div>
					<br>
				</div>
				<div class="col-lg-3">

					<img class="rounded"
						src="<c:url value="/resources/image/demo4.png"/>" width="250"
						height="150" alt="demo4">
				</div>
				<div class="col-lg-3">
					<img class="rounded"
						src="<c:url value="/resources/image/demo5.png"/>" width="250"
						height="150" alt="demo5">
				</div>
				<div class="col-lg-3">
					<img class="rounded"
						src="<c:url value="/resources/image/demo6.png"/>" width="250"
						height="150" alt="demo6">
				</div>
				<div class="col-lg-3">
					<img class="rounded"
						src="<c:url value="/resources/image/demo7.png"/>" width="250"
						height="150" alt="demo7">
				</div>
				<c:set value="0" var="count"></c:set>
				<div class="col-lg-12">
					<div class="text-center">
						<a href="#"><h3 class="text-info">SALE</h3></a>
					</div>

					<div id="carousel-sale" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ul class="carousel-indicators">
							<li data-target="#carousel-sale" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-sale" data-slide-to="1"></li>
							<li data-target="#carousel-sale" data-slide-to="2"></li>
						</ul>
						<!-- The slideshow -->
						<div class="carousel-inner">

							<div class="carousel-item active">
								<div class="row" id="carousel-sale-row1">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">

											<a href="${listpro[count].pro_id}"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/> " width="270"
												height="200" alt="Card image cap"> </a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost}</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1}" var="count"></c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count"></c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost}</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1}" var="count"></c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id}"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1}" var="count"></c:set>
								</div>

							</div>
							<div class="carousel-item">
								<div class="row" id="carousel-sale-row2">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>

								</div>
							</div>
							<div class="carousel-item">
								<div class="row" id="carousel-sale-row3">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
								</div>
							</div>
						</div>
						<!-- Left and right controls -->
						<a class="carousel-control-prev" href="#carousel-sale"
							data-slide="prev"> <span class="carousel-control-prev-icon"
							style="color: green"> </span>
						</a> <a class="carousel-control-next" href="#carousel-sale"
							data-slide="next"> <span class="carousel-control-next-icon"></span>
						</a>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="text-center">
						<a href="#"><h3 class="text-info">Technology</h3></a>
					</div>
					<div id="carousel-technology" class="carousel slide"
						data-ride="carousel">
						<!-- Indicators -->
						<ul class="carousel-indicators">
							<li data-target="#carousel-technology" data-slide-to="0"
								class="active"></li>
							<li data-target="#carousel-technology" data-slide-to="1"></li>
							<li data-target="#carousel-technology" data-slide-to="2"></li>
						</ul>
						<!-- The slideshow -->
						<div class="carousel-inner">
							<div class="carousel-item active">
								<div class="row" id="carousel-tech-row1">

									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
								</div>
							</div>
							<div class="carousel-item">
								<div class="row" id="carousel-tech-row2">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>

								</div>
							</div>
							<div class="carousel-item">
								<div class="row" id="carousel-tech-row3">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
								</div>
							</div>
						</div>
						<!-- Left and right controls -->
						<a class="carousel-control-prev" href="#carousel-technology"
							data-slide="prev"> <span class="carousel-control-prev-icon"></span>
						</a> <a class="carousel-control-next" href="#carousel-technology"
							data-slide="next"> <span class="carousel-control-next-icon"></span>
						</a>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="text-center">
						<a href="#"><h3 class="text-info">Fashion</h3></a>
					</div>
					<div id="carousel-fashion" class="carousel slide"
						data-ride="carousel">
						<!-- Indicators -->
						<ul class="carousel-indicators">
							<li data-target="#carousel-fashion" data-slide-to="0"
								class="active"></li>
							<li data-target="#carousel-fashion" data-slide-to="1"></li>
							<li data-target="#carousel-fashion" data-slide-to="2"></li>
						</ul>
						<!-- The slideshow -->
						<div class="carousel-inner">
							<div class="carousel-item active">
								<div class="row" id="carousel-Fashion-row1">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
								</div>

							</div>
							<div class="carousel-item">
								<div class="row" id="carousel-Fashion-row2">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>

								</div>
							</div>
							<div class="carousel-item">
								<div class="row" id="carousel-Fashion-row3">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
								</div>
							</div>
						</div>
						<!-- Left and right controls -->
						<a class="carousel-control-prev" href="#carousel-fashion"
							data-slide="prev"> <span class="carousel-control-prev-icon"></span>
						</a> <a class="carousel-control-next" href="#carousel-fashion"
							data-slide="next"> <span class="carousel-control-next-icon"></span>
						</a>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="text-center">
						<a href="#"><h3 class="text-info">Book</h3></a>
					</div>
					<div id="carousel-book" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ul class="carousel-indicators">
							<li data-target="#carousel-book" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-book" data-slide-to="1"></li>
							<li data-target="#carousel-book" data-slide-to="2"></li>
						</ul>
						<!-- The slideshow -->
						<div class="carousel-inner">
							<div class="carousel-item active">
								<div class="row" id="carousel-Book-row1">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
								</div>

							</div>
							<div class="carousel-item">
								<div class="row" id="carousel-Book-row2">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>

								</div>
							</div>
							<div class="carousel-item">
								<div class="row" id="carousel-Book-row3">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
								</div>
							</div>
						</div>
						<!-- Left and right controls -->
						<a class="carousel-control-prev" href="#carousel-book"
							data-slide="prev"> <span class="carousel-control-prev-icon"></span>
						</a> <a class="carousel-control-next" href="#carousel-book"
							data-slide="next"> <span class="carousel-control-next-icon"></span>
						</a>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="text-center">
						<a href="#"><h3 class="text-info">Home and furniture</h3></a>
					</div>
					<div id="carousel-homefurniture" class="carousel slide"
						data-ride="carousel">
						<!-- Indicators -->
						<ul class="carousel-indicators">
							<li data-target="#carousel-homefurniture" data-slide-to="0"
								class="active"></li>
							<li data-target="#carousel-homefurniture" data-slide-to="1"></li>
							<li data-target="#carousel-homefurniture" data-slide-to="2"></li>
						</ul>
						<!-- The slideshow -->
						<div class="carousel-inner">
							<div class="carousel-item active">
								<div class="row" id="carousel-HAF-row1">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
								</div>

							</div>
							<div class="carousel-item">
								<div class="row" id="carousel-HAF-row2">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>

								</div>
							</div>
							<div class="carousel-item">
								<div class="row" id="carousel-HAF-row3">
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
									<div class="column">
										<div class="card" style="width: 280px" style="height: 50px">
											<a href="${listpro[count].pro_id }"><img
												class="card-img-top"
												src="<c:url value="${listimg[count].img }"/>" width="270"
												height="200" alt="Card image cap"></a>
											<div class="card-body">
												<p class="card-text">Price: ${listpro[count].pro_cost }</p>
											</div>
										</div>
									</div>
									<c:set value="${count+1 }" var="count">
									</c:set>
								</div>
							</div>
						</div>
						<!-- Left and right controls -->
						<a class="carousel-control-prev" href="#carousel-homefurniture"
							data-slide="prev"> <span class="carousel-control-prev-icon"></span>
						</a> <a class="carousel-control-next" href="#carousel-homefurniture"
							data-slide="next"> <span class="carousel-control-next-icon"></span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid" id="footer-top">
		<div class="row" style="display: flex; flex-direction: column;">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links">
				<div class="links-in-footer">
					<p class="footer-title">INSTRUCTION</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">SERVICES</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">LINKED PAGE</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">PAYMENT</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">HOTLINE</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links-collapse">
				<div id="accordion">
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseOne">INSTRUCTION</p>
						<div id="collapseOne" class="collapse show"
							data-parent="#accordion">
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseTwo">SERVICES</p>
						<div id="collapseTwo" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse"
							href="#collapseThree">LINKED PAGES</p>
						<div id="collapseThree" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
				<div class="container" id="footer-bottom">
					<div class="footer-bottom-top">
						<div>
							<p>
								<i class="fa fa-store" style="font-size: 50px;"></i>
							</p>
							<br>
							<p style="margin-top: -40px; font-weight: 700;">Green Academy</p>
						</div>

						<div class="footer-bottom-top-right">
							<button type="button" id="btnFace">
								<i class="fab fa-facebook-f"></i>
							</button>
							<button type="button" id="btnIns">
								<i class="fab fa-instagram"></i>
							</button>
							<button type="button" id="btnYou">
								<i class="fab fa-youtube"></i>
							</button>
						</div>
					</div>
					<div class="footer-bottom-bottom">
						<div
							style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
							<div class="contact">
								<p style="margin-bottom: 2px;">Website:
									www.fashionstar.company</p>
								<p style="margin-bottom: 2px;">Äá»a chá»: 212A2 Nguyá»n
									TrÃ£i, P.Nguyá»n CÆ° Trinh, Q.1, Tp.Há» ChÃ­ Minh</p>
								<p style="margin-bottom: 2px;">[Email]:
									support@fashionstar.vn - [Hotline]: 1900 636 820 -[Tel]: (028)
									3925 5050 Ext 200 -[Fax]: (028) 3925 5050. Báº£n quyá»n Â©
									2015 cá»§a Fashion Star. Táº¥t cáº£ cÃ¡c quyá»n ÄÆ°á»£c báº£o
									lÆ°u</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- At here -->
</body>
</html>