<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/viewProduct.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/cartPage.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/cartPage.css" />">

<script type="text/javascript">
</script>
<style type="text/css">
</style>
<title>Order List</title>
</head>
<body style="background-color: #F4F4F4;"
	onload="setNumItemCart(${cartItems})">
	<div id="header">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="../homecontroller/homepage"><i
						class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link"
									href="../homecontroller/homepage"><i
										class="fa fa-fw fa-home"></i>Home</a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i>Category</a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<a class="dropdown-item" href="#">Technology</a> <a
												class="dropdown-item" href="#">Fashion</a> <a
												class="dropdown-item" href="#">Food and Drink</a>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link" href="#"><i
										class="fa fa-fw fa-envelope"></i>Contact us</a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i>Languages</a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">Sign
										in/Sign up</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link" href="#">VN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">EN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">LA</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">
					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>

			</div>

			<c:if test="${currentUser == null}">
				<button
					onclick="location.href='../viewProductController/movetologin';"
					class="btn" id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span>Log in</span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${fn:substring(currentUser.user_name, 0, 5)}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="../orderListController/orderList">My
							order list</a> <a class="dropdown-item" href="#">Account detail</a>
						<c:if test="${role == 'admin'}">
							<a class="dropdown-item" href="../categoryController/categories">Admin
								page</a>
						</c:if>
						<a class="dropdown-item" href="../loginRegisterController/logout">Log
							out</a>
					</div>
				</div>

			</c:if>

			<button class="btn" id="btnCart" type="button" value="${user_id}"
				onclick="location.href='../cartController/cartPage'"
				style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
				<i class="fas fa-shopping-cart"><span class="badge badge-light"
					style="background-color: yellow; margin-left: 5px;"
					id="num-item-in-cart">0</span></i>
			</button>


			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>

	<div id="content" class="container"
		style="display: flex; flex-direction: column; margin-top: 100px; border-bottom-color: #F7F7F7;">

		<div id="accordion">
			<table class="table table-hover" align="center"
				style="width: 888.167px; text-align: center;">
				<tr>
					<th>Bill code</th>
					<th>Order date</th>
					<th>Item</th>
					<th>Total price</th>
				</tr>
			</table>
			<c:if test="${fn:length(bills) > 0}">
				<c:set var="count" value="0"></c:set>
				<c:forEach items="${bills}" var="bill">
					<div class="card">
						<div class="card-header">
							<table class="table table-hover" data-toggle="collapse"
								href="#collapseOne${bill.bill_id}">
								<tr style="text-align: center;">

									<th>${bill.bill_id}</th>
									<th>${bill.order_date}</th>
									<th>${fn:substring(items[count].product.pro_name, 0, 10)}</th>
									<th>${bill.total}</th>
								</tr>
							</table>
						</div>
						<div id="collapseOne${bill.bill_id}" class="collapse show"
							data-parent="#accordion">
							<div class="card-body">
								<table class="table table-hover" style="text-align: center;">
									<tr>
										<th>Product</th>
										<th>Price</th>
										<th>Quantity</th>
										<th>Price x Quantity</th>
									</tr>
									<c:forEach items="${billDetails}" var="billDetails">
										<c:if test="${bill.bill_id == billDetails.id.bill_id}">
											<tr>
												<td><img style="width: 50px; height: 50px;"
													src='<c:url value="${imgs[count].img}"/>'> | <a
													href="../viewProductController/product?proid=${items[count].product.pro_id}">${fn:substring(items[count].product.pro_name, 0, 10)}</a></td>
												<td>${items[count].product.pro_cost}</td>
												<td>${items[count].num}</td>
												<td>${items[count].product.pro_cost * items[count].num}</td>
											</tr>
											<c:set var="count" value="${count+1}"></c:set>
										</c:if>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</c:forEach>
			</c:if>
		</div>
	</div>
	<!-- ket thuc content -->

</body>
</html>