<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/viewProduct.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/cartPage.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/cartPage.css" />">

<script type="text/javascript">
</script>
<style type="text/css">
</style>
<title>Cart page</title>
</head>
<body style="background-color: #F4F4F4;"
	onload="setNumItemCart(${cartItems});initializeCheckOutContainer()">
	<div id="header">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="../homecontroller/homepage"><i
						class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link"
									href="../homecontroller/homepage"><i
										class="fa fa-fw fa-home"></i>Home</a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i>Category</a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<a class="dropdown-item" href="#">Technology</a> <a
												class="dropdown-item" href="#">Fashion</a> <a
												class="dropdown-item" href="#">Food and Drink</a>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link" href="#"><i
										class="fa fa-fw fa-envelope"></i>Contact us</a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i>Languages</a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">Sign
										in/Sign up</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link" href="#">VN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">EN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">LA</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">
					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>

			</div>

			<c:if test="${currentUser == null}">
				<button
					onclick="location.href='../viewProductController/movetologin';"
					class="btn" id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span>Log in</span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${fn:substring(currentUser.user_name, 0, 5)}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item"
							href="/final_project/orderListController/orderList">My order
							list</a> <a class="dropdown-item" href="#">Account detail</a>
						<c:if test="${role == 'admin'}">
							<a class="dropdown-item" href="../categoryController/categories">Admin
								page</a>
						</c:if>
						<a class="dropdown-item" href="../loginRegisterController/logout">Log
							out</a>
					</div>
				</div>

			</c:if>

			<button class="btn" id="btnCart" type="button" value="${user_id}"
				onclick="goInToCart()"
				style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
				<i class="fas fa-shopping-cart"><span class="badge badge-light"
					style="background-color: yellow; margin-left: 5px;"
					id="num-item-in-cart">0</span></i>
			</button>


			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>

	<div id="content" class="container"
		style="display: flex; flex-direction: column; margin-top: 100px; border-bottom-color: #F7F7F7;">
		<div id="cart-title">
			<h3>Shopping Cart</h3>
			<c:if test="${cartItems > 0}">
				<p style="color: #6581aa; float: left;" id="item-quantity-in-cart">${cartItems}</p>
				<p style="color: #6581aa; float: left;">&nbspitems in the bag</p>
			</c:if>
			<c:if test="${cartItems == 0}">
				<p style="color: #6581aa;">0 items in the bag</p>
			</c:if>
			<div id="alrert-is-removing">
				<button type="button" class="btn btn-primary" data-toggle="modal"
					data-target="#exampleModal" id="open-alert-remove-button"
					style="visibility: hidden;">Launch demo modal</button>

				<!-- Modal -->
				<div class="modal fade" id="exampleModal" tabindex="-1"
					role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header" style="background-color: #FAFAFA;">
								<h5 class="modal-title" id="exampleModalLabel">Warning</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body" style="text-align: center;">Do you
								really want to remove this item?</div>
							<div class="modal-footer" style="background-color: #FAFAFA;">
								<button type="button" class="btn btn-secondary"
									data-dismiss="modal" onclick="disagreeRemove(this)" value=""
									id="disagreeRemoveButton">No</button>
								<button type="button" class="btn btn-primary"
									onclick="agreeRemove(this)" value="" id="agreeRemoveButton"
									data-dismiss="modal">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="alrert-is-checkout">
				<!-- Modal -->
				<div class="modal fade" id="checkout" tabindex="-1" role="dialog"
					aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg"
						role="document" style="max-width: 1000px;">
						<div class="modal-content" style="height: 800px;">
							<div class="modal-header" style="background-color: #FAFAFA;">
								<h5 class="modal-title" id="exampleModalLabel"
									style="margin-left: auto;">Secure Checkout</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>


							<div class="modal-body"
								style="text-align: center; display: flex; flex-direction: row;"
								id="form-checkout">
								<form action="" style="display: flex; flex-direction: row;">
									<div id="info-shipping-form"
										style="width: 600px; /*  background-color: blue;  */ height: 100%; display: flex; flex-direction: column;">
										<h3 style="margin-bottom: 20px; display: inherit;">Shipping</h3>
										<div class="form-group">
											<label for="usr" hidden="hidden">Name:</label> <input
												name="fname" placeholder="First name" style="width: 300px"
												class="form-control" id="txtFName">
										</div>
										<div class="form-group">
											<label for="usr" hidden="hidden">Name:</label> <input
												name="lname" placeholder="Last name" style="width: 300px"
												class="form-control" id="txtLName">
										</div>
										<div class="form-group">
											<label for="usr" hidden="hidden">Name:</label> <input
												name="email" placeholder="Email" style="width: 300px"
												class="form-control" id="txtEmail">
										</div>
										<div class="form-group">
											<label for="usr" hidden="hidden">Name:</label> <input
												name="phone" placeholder="Phone" style="width: 300px"
												class="form-control" id="txtPhone">
										</div>

										<select id="city-select-tag" style="width: 300px"
											class="form-control">
											<option selected value="-1">-- Select an city --</option>
											<!-- <option value="volvo">Volvo</option>
											<option value="saab">Saab</optio	n>
											<option value="mercedes">Mercedes</option>
											<option value="audi">Audi</option> -->
										</select> <select id="district-select-tag"
											style="width: 300px; margin-top: 16px;" class="form-control">
											<option selected value="-1">-- Select an
												district/province --</option>
											<!-- <option value="volvo">Volvo</option>
											<option value="saab">Saab</option>
											<option value="mercedes">Mercedes</option>
											<option value="audi">Audi</option> -->
										</select>
									</div>
									<div id="info-left" style="width: 366.167px; height: 100%;">
										<table id="info-cart-table"
											style="width: 300px; border: 1px solid #cacaca; margin-left: auto; margin-right: auto;">
										</table>
									</div>
								</form>
							</div>

							<div class="modal-body"
								style="text-align: center; display: none; flex-direction: column;"
								id="form-notify">
								<h3>You ordered successfully!!!</h3>
								<table id="table-notify" style="text-align: left" align="center">
									<tr>
										<td>Customer name :</td>
										<td style="float: right;" id="name">${currentUser.user_name}</td>
									</tr>
									<tr>
										<td>Customer phone :</td>
										<td style="float: right;" id="phone">${currentUser.phone}</td>
									</tr>
									<tr>
										<td>Customer email :</td>
										<td style="float: right;" id="email">${currentUser.email}</td>
									</tr>
									<tr>
										<td>Shipping address :</td>
										<td id="shippingAddress" style="float: right;"></td>
									</tr>
									<!-- <tr>
										<td>Summary :</td>
									</tr> -->
								</table>
							</div>

							<div class="modal-footer" style="background-color: #FAFAFA;">
								<button id="buy-button" type="button" class="btn btn-primary"
									onclick="buy(this)" value="" id="agreeRemoveButton">Choose
									Shipping</button>
								<button style="display: none;" id="close-button" type="button"
									class="btn btn-primary" onclick="location.href='./cartPage'"
									value="" id="agreeRemoveButton">Close</button>
								<!-- data-dismiss="modal" -->
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div id="listCart">
			<table id="cart-table" style="width: 1110px;">
				<c:set var="count" value="0"></c:set>
				<c:set var="total" value="0"></c:set>
				<c:forEach items="${listItems}" var="item" begin="0">
					<tr
						style="border-bottom: 1px solid #efefef; border-top: 1px solid #efefef; height: 100px;">
						<td><img style="width: 50px; height: 50px;"
							src='<c:url value="${listImage[count].img}"/>'></td>
						<td
							style="display: flex; flex-direction: column; justify-content: space-around; height: 100px">
							<div
								style="display: flex; flex-direction: column; justify-content: space-around; height: 100px;">
								<div style="margin-top: 15px;">
									<a class="product-title"
										href="../viewProductController/product?proid=${item.product.pro_id}"
										style="text-transform: uppercase; text-decoration: none; font-weight: 700;">${item.product.pro_name}</a><br>
									<p class="product-cost" style="float: left;">${item.product.pro_cost*item.num}</p>
									<c:set var="total"
										value="${total+item.product.pro_cost*item.num}"></c:set>
									<span style="float: left;">&#8363</span>
								</div>
							</div>
						</td>
						<td>
							<div style="display: inline-flex;">
								<button class="btn btn-outline-secondary"
									onclick="descrease(${item.product.pro_id}, ${count})"
									value="${count}">
									<i class="fas fa-minus"></i>
								</button>
								<input value="${item.num}"
									style="width: 39.8333px; height: 37.8333px; background-color: #f4f4f4; text-align: center; border-radius: .2rem; border: 1px solid #6c757d;">
								<button class="btn btn-outline-secondary"
									onclick="inscrease(${item.product.pro_id}, ${count})">
									<i class="fas fa-plus"></i>
								</button>
							</div>
						</td>
						<td><button class="remove-item"
								style="border: none; background-color: #f4f4f4;"
								onclick="deleteItem(${item.product.pro_id}, ${count}, this)"
								value="${count}">
								<i class="far fa-trash-alt remove-item" style="cursor: pointer;"></i>
							</button></td>
					</tr>
					<c:set var="count" value="${count+1}"></c:set>
				</c:forEach>
			</table>
			<script type="text/javascript">
			function descrease(pro_id, order) {
				changeWhenDescrease(pro_id, order);
			}
			function inscrease(pro_id, order) {
				changeWhenInscrease(pro_id, order);
			}
			function deleteItem(pro_id, order, button){
				implementDeleteItem(pro_id, order, button);
				}
			</script>
		</div>
		<div id="check-out-container"
			style="display: flex; flex-direction: row; justify-content: space-between;">
			<p style="color: #6581aa; margin-top: 10px; visibility: hidden;"
				id="empty-cart-inform">It appears that your cart is currently
				empty.</p>
			<div style="display: flex; flex-direction: column;">
				<p style="color: #6581aa; margin-top: 10px; visibility: hidden;"
					id="total-container">
					Total :<span id="total-price"
						style="font-size: 30px; font-weight: 700; color: black;">${total}</span><span
						style="font-size: 30px; font-weight: 700; color: black;">&#8363</span>
				</p>

				<button type="button" id="check-out-button" class="btn btn-dark"
					style="visibility: hidden;" data-toggle="modal"
					data-target="#checkout"
					onclick="getCities();getInfoCartForCheckOut()">CHECK OUT</button>

				<button type="button" id="keep-shopping-button" class="btn btn-dark"
					style="visibility: hidden;"
					onclick="location.href='../views/homePage.jsp'">KEEP
					SHOPPING</button>
			</div>
		</div>
	</div>

	<div class="container-fluid" id="footer-top">
		<div class="row" style="display: flex; flex-direction: column;">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links">
				<div class="links-in-footer">
					<p class="footer-title">INSTRUCTION</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">SERVICES</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">LINKED PAGE</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">PAYMENT</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">HOTLINE</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links-collapse">
				<div id="accordion">
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseOne">INSTRUCTION</p>
						<div id="collapseOne" class="collapse show"
							data-parent="#accordion">
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseTwo">SERVICES</p>
						<div id="collapseTwo" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse"
							href="#collapseThree">LINKED PAGES</p>
						<div id="collapseThree" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
				<div class="container" id="footer-bottom">
					<div class="footer-bottom-top">
						<div>
							<p>
								<i class="fa fa-store" style="font-size: 50px;"></i>
							</p>
							<br>
							<p style="margin-top: -40px; font-weight: 700;">Green Academy</p>
						</div>

						<div class="footer-bottom-top-right">
							<button type="button" id="btnFace">
								<i class="fab fa-facebook-f"></i>
							</button>
							<button type="button" id="btnIns">
								<i class="fab fa-instagram"></i>
							</button>
							<button type="button" id="btnYou">
								<i class="fab fa-youtube"></i>
							</button>
						</div>
					</div>
					<div class="footer-bottom-bottom">
						<div
							style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
							<div class="contact">
								<p style="margin-bottom: 2px;">Website:
									www.fashionstar.company</p>
								<p style="margin-bottom: 2px;">Äá»a chá»: 212A2 Nguyá»n
									TrÃ£i, P.Nguyá»n CÆ° Trinh, Q.1, Tp.Há» ChÃ­ Minh</p>
								<p style="margin-bottom: 2px;">[Email]:
									support@fashionstar.vn - [Hotline]: 1900 636 820 -[Tel]: (028)
									3925 5050 Ext 200 -[Fax]: (028) 3925 5050. Báº£n quyá»n Â©
									2015 cá»§a Fashion Star. Táº¥t cáº£ cÃ¡c quyá»n ÄÆ°á»£c báº£o
									lÆ°u</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

<!-- localhost:8181/final_project/viewProductController/product?proid=1&userid=3 -->