<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- cop -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">
<script type="text/javascript"
	src="<c:url value="/resources/js/header_footer/header_footer.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/viewProduct.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/header_footer/header_footer.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/css/viewProduct.css" />">


<style type="text/css">
#table-more-detail th {
	background-color: #EFEFEF;
	width: 200px;
	border: 1px solid #e6e6e6;
}

#table-more-detail th, td {
	padding-left: 10px;
}

#table-more-detail tr {
	border: 1px solid #e6e6e6;
}

#table_question-answer p {
	font-size: 14px;
}

.mySlides {
	display: none;
}

.jumbotron {
	background-color: #f4511e;
	color: #fff;
}
</style>
<title>View Product</title>
</head>
<body style="background-color: #F4F4F4;"
	onload="changeStarColorViewProduct(${product.pro_star});separatePageViewed(${fn:length(listViewed)})">
	<div id="header">
		<nav
			class="navbar navbar-expand-md bg-light navbar-light flex-row justify-content-between fixed-top"
			id="navbar">
			<div>
				<nav
					class="navbar navbar-expand-sm bg-light navbar-light sticky-top">
					<a class="navbar-brand" href="../homecontroller/homepage"><i
						class="fa fa-store"></i>TATALO</a>
					<div class="collapse navbar-collapse" id="collapsibleNavbar">
						<div class="narbar-brand">
							<ul class="navbar-nav">
								<li class="nav-item"><a class="nav-link"
									href="../homecontroller/homepage"><i
										class="fa fa-fw fa-home"></i>Home</a></li>
								<li class="nav-item">
									<div class="dropdown"
										style="background-color: #F8F9FA; border: none;">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown"
											id="demo"><i class="fa fa-fw fa-align-justify"></i>Category</a>
										<div class="dropdown-menu" style="background-color: inherit;">
											<a class="dropdown-item" href="#">Technology</a> <a
												class="dropdown-item" href="#">Fashion</a> <a
												class="dropdown-item" href="#">Food and Drink</a>
										</div>
									</div>
								</li>
								<li class="nav-item"><a class="nav-link" href="#"><i
										class="fa fa-fw fa-envelope"></i>Contact us</a></li>
								<li class="nav-item"><a class="nav-link navbar-toggler"
									href="#" data-toggle="collapse" data-target="#languages"
									style="border: none;" id="lang"><i
										class="fas fa-fw fa-globe-europe"></i>Languages</a></li>
								<li class="nav-item navbar-toggler" style="border: none;"><button
										class="btn" id="signIn" type="button"
										style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: -10px; margin-top: 5px;">Sign
										in/Sign up</button></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>

			<div class="collapse navbar-collapse justify-content-end"
				id="languages">
				<ul class="navbar-nav ">
					<li class="nav-item"><a class="nav-link" href="#">VN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">EN</a></li>
					<li class="nav-item"><a class="nav-link" href="#">LA</a></li>
				</ul>
				<form class="form-inline" action="/action_page.php">

					<input class="form-control mr-sm-2" type="text"
						placeholder="Search"
						style="background-color: inherit; display: none;" id="searchBox">
					<button class="btn btn-success" type="submit"
						style="background-color: inherit; border: 0px solid #e6e6e6; color: #999999;"
						id="btnSearch">
						<i class="fas fa-search"></i>
					</button>
				</form>
			</div>

			<c:if test="${currentUser == null}">
				<button
					onclick="location.href='../viewProductController/movetologin';"
					class="btn" id="signInOut" type="button"
					style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
					<i class="fas fa-user"></i> <span>Log in</span>
				</button>
			</c:if>
			<c:if test="${currentUser != null}">
				<div class="dropdown">
					<button class="btn" id="signInOut" type="button"
						data-toggle="dropdown"
						style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px; font-size: 14px;">
						<i class="fas fa-user"></i><span id="btn-account-user-name">${fn:substring(currentUser.user_name, 0, 5)}</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item"
							href="/final_project/orderListController/orderList">My order
							list</a> <a class="dropdown-item" href="#">Account detail</a>
						<c:if test="${role == 'admin'}">
							<a class="dropdown-item" href="../categoryController/categories">Admin
								page</a>
						</c:if>
						<a class="dropdown-item" href="../loginRegisterController/logout">Log
							out</a>
					</div>
				</div>
			</c:if>

			<button class="btn" id="btnCart" type="button" value="${user_id}"
				onclick="goInToCart()"
				style="background-color: inherit; border: 1px solid #e6e6e6; color: #999999; margin-left: 5px;">
				<i class="fas fa-shopping-cart"><span class="badge badge-light"
					style="background-color: yellow; margin-left: 5px;"
					id="num-item-in-cart">0</span></i>
			</button>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
		</nav>
	</div>
	<div id=content class="container" style="margin-top: 100px;">
		<div class="jumbotron text-center" style="margin-bottom: 0">
			<h1 class="text-center">TATALO</h1>
			<p>Make your life easier</p>
		</div>
		<div class="row">
			<div class="col-lg-8">
				<form>
					<div class="form-group">
						<label>Name</label> <input type="text" class="form-control"
							id="name" name="name">
					</div>
					<div class="form-group">
						<label>Email</label> <input type="text" class="form-control"
							id="email" name="email">
					</div>
					<div class="form-group">
						<label for="comment">Comment:</label>
						<textarea class="form-control" rows="5" id="comment" name="text"></textarea>
					</div>
					<a href="#" class="btn btn-success">Summit</a>
				</form>
			</div>
			<div class="col-lg-4 ">
				<p></p>
				<br>
				<h5>Contact us and we'll get back to you within 24 hours.</h5>
				<p>
					<i class='fas fa-address-card'></i> Chicago, US
				</p>
				<p>
					<i class='fas fa-phone'></i> +00 1515151515
				</p>
				<p>
					<i class='fas fa-mail-bulk'></i> myemail@something.com
				</p>
			</div>
		</div>
	</div>
	<div class="container-fluid" id="footer-top">
		<div class="row" style="display: flex; flex-direction: column;">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links">
				<div class="links-in-footer">
					<p class="footer-title">INSTRUCTION</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">SERVICES</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">LINKED PAGE</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">PAYMENT</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
				<div class="links-in-footer">
					<p class="footer-title">HOTLINE</p>
					<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a href="#">d</a>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12"
				id="footer-links-collapse">
				<div id="accordion">
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseOne">INSTRUCTION</p>
						<div id="collapseOne" class="collapse show"
							data-parent="#accordion">
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse" href="#collapseTwo">SERVICES</p>
						<div id="collapseTwo" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
					<div>
						<p class="footer-title" data-toggle="collapse"
							href="#collapseThree">LINKED PAGES</p>
						<div id="collapseThree" class="collapse show"
							data-parent="#accordion">
							<hr>
							<div class="links-in-footer">
								<a href="#">a</a> <a href="#">b</a> <a href="#">c</a> <a
									href="#">d</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12 col-xm-12">
				<div class="container" id="footer-bottom">
					<div class="footer-bottom-top">
						<div>
							<p>
								<i class="fa fa-store" style="font-size: 50px;"></i>
							</p>
							<br>
							<p style="margin-top: -40px; font-weight: 700;">Green Academy</p>
						</div>

						<div class="footer-bottom-top-right">
							<button type="button" id="btnFace">
								<i class="fab fa-facebook-f"></i>
							</button>
							<button type="button" id="btnIns">
								<i class="fab fa-instagram"></i>
							</button>
							<button type="button" id="btnYou">
								<i class="fab fa-youtube"></i>
							</button>
						</div>
					</div>
					<div class="footer-bottom-bottom">
						<div
							style="display: flex; flex-direction: column; justify-content: space-around; background-color: '';">
							<div class="contact">
								<p style="margin-bottom: 2px;">Website:
									www.fashionstar.company</p>
								<p style="margin-bottom: 2px;">Äá»a chá»: 212A2 Nguyá»n
									TrÃ£i, P.Nguyá»n CÆ° Trinh, Q.1, Tp.Há» ChÃ­ Minh</p>
								<p style="margin-bottom: 2px;">[Email]:
									support@fashionstar.vn - [Hotline]: 1900 636 820 -[Tel]: (028)
									3925 5050 Ext 200 -[Fax]: (028) 3925 5050. Báº£n quyá»n Â©
									2015 cá»§a Fashion Star. Táº¥t cáº£ cÃ¡c quyá»n ÄÆ°á»£c báº£o
									lÆ°u</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- At here -->
</body>
</html>