<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Product Management</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/admin_Product.css" />">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body onload="">
	<header style="float: left;">
		<nav class="navbar navbar-expand-sm">
			<a class="navbar-brand" href="../homecontroller/homepage">TATALO</a>
			<button class="navbar-toggler d-lg-none" type="button"
				data-toggle="collapse" data-target="#collapsibleNavId"
				aria-controls="collapsibleNavId" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="collapsibleNavId">
				<ul class="navbar-nav ">
					<li class="nav-item active mr-2"><i class="fa fa-tasks"></i></li>
					<li class="nav-item mr-2"><i class="fa fa-envelope-o"></i></li>
					<li class="nav-item mr-2"><i class="fa fa-bell-o"></i></li>
				</ul>
			</div>
			<div class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="dropdownId"
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
				<div class="dropdown-menu" aria-labelledby="dropdownId">
					<a class="dropdown-item" data-toggle="modal"
						data-target="#statementModal">Statement</a> <a
						class="dropdown-item" href="#">Exit</a>
				</div>
			</div>
		</nav>
	</header>

	<div id="content" style="display: flex;">
		<div class="wrapper d-flex justify content-between">
			<div id="side-bar">
				<div class="logo">ADMIN PAGE</div>
				<ul class="list-group rounded-0">
					<li class="dashboard">DASHBOARD</li>
					<li><a href="../categoryController/categories"> <i
							class="fa fa-book mr-2"></i> Catalog management
					</a></li>
					<li><a href="../productController/products"> <i
							class="fa fa-user mr-2"></i> Product management
					</a></li>
					<li><a href="../userController/users"><i class="fa fa-cart-plus"></i> User
							management </a></li>
					<li><a href="#"><i class="fa fa-tags"></i> Customer
							message </a></li>
					<li><a href="#"><i class="fa fa-star"></i> Show the
							gathered points </a></li>
				</ul>
			</div>
		</div>

		<div style="margin-top: 60px;">
			<div id="wrapper-content" style="">
				<section class="control">
					<div class="row" style="margin-left: 1px; padding: 0 5px;">
						<div class="d-flex align-items-center ">
							<div class="input-group">
								<input type="text" class="form-control"
									placeholder="Name Catalog" id="searchName">
								<div class="input-group-prepend">
									<span class="input-group-text" id="btnTimNV"><i
										class="fa fa-search"></i></span>
								</div>
							</div>
						</div>
						<div class="align-items-center justify-content-end"
							style="display: flex; flex-direction: row; margin: auto; width: 400px;">
							<label style="width: 100px">Sort</label> <select id="mySeclect"
								style="width: 300px;">
								<option value="ten">Follow Name</option>
								<option value="id">Follow ID</option>
							</select>
							<form id="myRadioBtn" class="d-flex align-items-center m-2"
								action="">
								<input class="m-2" type="radio" name="gender" value="tang">Increase<br>
								<input class="m-2" type="radio" name="gender" value="giam">Decrease<br>
							</form>
						</div>
					</div>



					<button class="btn btn-primary" data-toggle="modal"
						data-target="#myModal" style="float: right;">Add</button>

					<!-- <div style="display: none;" id="test"> -->
					<h2>Add Product</h2>
					<div class="modal fade" id="myModal">
						<div class="modal-dialog">
							<form:form action="./product" method="post"
								modelAttribute="product">
								<div class="modal-content">

									<!-- Modal Header -->
									<div class="modal-header">
										<h4 class="modal-title" style="text-align: center;">Product Detail</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>

									<!-- Modal body -->
									<div class="modal-body">

										<fieldset>
											<div class="control-group">
												<label class="control-label" for="txtProductName">Name
													Product</label>
												<form:input type="text" path="pro_name" class="form-control"
													id="txtProductName" placeholder="Product Name" />
											</div>
											<div class="control-group">
												<label class="control-label" for="txtProductCost">Cost</label>
												<form:input type="text" path="pro_cost" class="form-control"
													id="txtProductCost" placeholder="Cost" />
											</div>
											<div class="control-group">
												<label class="control-label" for="txtProductQuantity">Quantity</label>
												<form:input type="text" path="pro_quantity"
													class="form-control" id="txtProductQuantity"
													placeholder="Quantity" />
											</div>
											<div class="control-group">
												<label class="control-label" for="categories">Category</label>
												<form:select path="category.cate_id" id="category-select">
													<form:option value="-1" label="--- Select ---" />
													<form:options items="${categories}" itemValue="cate_id"
														itemLabel="cate_name" onclick="test(this)" id="cate_id"/>
												</form:select>
											</div>
											<div class="control-group">
												<label class="control-label" for="fileInput">File
													input</label>
												<div class="controls">
													<input class="input-file uniform_on" id="fileInput"
														type="file">
												</div>
											</div>
											<div class="control-group">
												<form:label path="pro_content">Description</form:label>
												<form:textarea path="pro_content" rows="5" cols="30" id="txtProductContent"/>
											</div>
										</fieldset>
									</div>
									<!-- Modal footer -->
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary"
											name="btnSaveInModal" value="" id="btnSaveInModal">Save changes</button>
										<button type="button" class="btn btn-danger"
											data-dismiss="modal">Close</button>
									</div>
								</div>
							</form:form>
						</div>
					</div>
					<!-- </div> -->


					<section class="tableCatalog">
						<table class="table table-bordered" style="width: 900px;"
							id="products-table">
							<c:set var="count" value="1"></c:set>
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Quantity</th>
									<th>Content</th>
									<th>Star</th>
									<th>Price</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${products}" var="product">
									<tr>
										<td>${product.pro_id}</td>
										<td>${product.pro_name}</td>
										<td>${product.pro_quantity}</td>
										<td>${product.pro_content}</td>
										<td>${product.pro_star}</td>
										<td>${product.pro_cost}</td>
										<td><button type="button" class="btn btn-primary"
												data-toggle="modal" data-target="#myModal"
												value="${product.pro_name}"
												onclick="update(this, ${product.pro_id})">Update</button>
											<button class="btn btn-danger" type="button"
												onclick="deleteProduct(this, ${product.pro_id})" value="${count}">Delete</button>

										</td>
									</tr>
									<c:set var="count" value="${count+1}"></c:set>
								</c:forEach>
							</tbody>
						</table>
					</section>
				</section>
			</div>
		</div>
	</div>
	<script>
	function deleteProduct(button, pro_id){
		console.log(pro_id);
		$.ajax({
			url : "./product/" + pro_id,
			type : "DELETE",
			data : "",
			contentType : "application/json",
			success : function(){
				var table = document.getElementById("products-table");
				table.deleteRow(button.value);
				//
				var tr = table.getElementsByTagName("tr");
				for (var i = 1; i < tr.length; i++){
					tr[i].getElementsByTagName("button")[1].value = i;
				}
			}
		});
	}
	function update(button, pro_id){
		$.ajax({
			url : "./aproduct?proId=" + pro_id,
			type : "GET",
			data : "",
			contentType : "application/json",
			success : function(data){
				document.getElementById("btnSaveInModal").value = "update-"+pro_id;
				
				document.getElementById("txtProductName").value = data[0].pro_name;
				document.getElementById("txtProductCost").value = data[0].pro_cost;
				document.getElementById("txtProductQuantity").value = data[0].pro_quantity;
				document.getElementById("txtProductContent").value = data[0].pro_content;
				var select = document.getElementById("category-select");
				/* for (var i = 0; i < select.getElementsByTagName("option").length; i++){
					console.log(select.getElementsByTagName("option")[i].id);
					} */
				document.getElementById("cate_id"+data[1].cate_id).selected = "true";
			},
			async : false,
			error : function(e) {
				console.log(e);
			}
			})
		
		/* console.log(document.getElementById("btnSaveInModal").value);
		document.getElementById("txtProductName").value = button.value;
		document.getElementById("txtProductCost").value = product.pro_cost; */
	}
	</script>
	<script type="text/javascript">
		
	</script>
</body>
</html>