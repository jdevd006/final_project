<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Quản lý Danh mục</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="<c:url value="/resources/css/admin_Catalog.css" />">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/admin_Catalog.js"/>"></script>
<script src="https://code.jquery.com/jquery-3.4.0.js"
	integrity="sha256-DYZMCC8HTC+QDr5QNaIcfR7VSPtcISykd+6eSmBW5qo="
	crossorigin="anonymous"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-sm">
			<a class="navbar-brand" href=../homecontroller/homepage">TATALO</a>
			<button class="navbar-toggler d-lg-none" type="button"
				data-toggle="collapse" data-target="#collapsibleNavId"
				aria-controls="collapsibleNavId" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="collapsibleNavId">
				<ul class="navbar-nav ">
					<li class="nav-item active mr-2"><i class="fa fa-tasks"></i></li>
					<li class="nav-item mr-2"><i class="fa fa-envelope-o"></i></li>
					<li class="nav-item mr-2"><i class="fa fa-bell-o"></i></li>
				</ul>
			</div>
			<div class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="dropdownId"
					data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
				<div class="dropdown-menu" aria-labelledby="dropdownId">
					<a class="dropdown-item" data-toggle="modal"
						data-target="#statementModal">Statement</a> <a
						class="dropdown-item" href="#">Exit</a>
				</div>
			</div>
		</nav>
	</header>
	<div class="wrapper d-flex justify content-between">
		<div id="side-bar">
			<div class="logo">ADMIN PAGE</div>
			<ul class="list-group rounded-0">
				<li class="dashboard">DASHBOARD</li>
				<li><a href="../categoryController/categories"> <i
						class="fa fa-book mr-2"></i> Catalog management
				</a></li>
				<li><a href="../productController/products"> <i
						class="fa fa-user mr-2"></i>Product management
				</a></li>
				<li><a href="../userController/users"><i
						class="fa fa-cart-plus"></i>User management</a></li>
				<li><a href="#"><i class="fa fa-tags"></i> Customer message
				</a></li>
				<li><a href="#"><i class="fa fa-star"></i> Show the
						gathered points </a></li>
			</ul>
		</div>
		<div id="wrapper-content">
			<!-- Nút thêm và tìm kiếm -->
			<section class="control">
				<div class="row">
					<div class="col md-5 d-flex align-items-center ">
						<div class="input-group">
							<input type="text" class="form-control"
								placeholder="Name Catalog" id="searchName">
							<div class="input-group-prepend">
								<span class="input-group-text" id="btnTimNV"><i
									class="fa fa-search"></i></span>
							</div>
						</div>
					</div>
					<div class="col md-6 d-flex align-items-center justify-content-end">
						<label style="width: 100%" style="padding-top: 6px;">Sort</label>
						<select id="mySeclect" style="margin-left: 10px;">
							<option value="ten">Follow Name</option>
							<option value="id">Follow ID</option>
						</select>
						<form id="myRadioBtn" class="d-flex align-items-center m-2"
							action="">
							<input class="m-2" type="radio" name="gender" value="tang">Increase<br>
							<input class="m-2" type="radio" name="gender" value="giam">Decrease<br>
						</form>
					</div>
					<div class="col md-1 d-flex justify-content-end">
						<button id="btnAdd" class="btn btn-primary" data-toggle="modal"
							data-target="#myModal">Add</button>
					</div>
				</div>
				<!-- The Modal -->
				<div class="modal fade" id="myModal">
					<div class="modal-dialog">
						<div class="modal-content">

							<!-- Modal Header -->
							<div class="modal-header">
								<h4 class="modal-title">Add catalog</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<!-- Modal body -->
							<div class="modal-body">
								<form:form action="./category" class="form-horizontal"
									method="post" modelAttribute="category">
									<form:input path="cate_id" type="hidden" />
									<div class="form-group">
										<label for="txtCateName" class="control-lable col-md-4">Name
											catalog</label>
										<form:input type="text" path="cate_name" class="form-control"
											id="txtCateName" placeholder="Category name" />
									</div>

									<div class="modal-footer">
										<div
											style="display: flex; flex-direction: row; justify-content: space-between;">
											<button id="btnSaveInModal" type="submit"
												class="btn btn-primary" value="" name="btnSaveInModal">Save</button>
										</div>
										<div
											style="display: flex; flex-direction: row; justify-content: space-between;">
											<button type="button" class="btn btn-danger"
												data-dismiss="modal">Close</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Tablle  -->
			<section class="tableCatalog">
				<c:set var="count" value="1"></c:set>
				<table class="table table-bordered" id="categories-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<!-- <th>Quantity</th> -->
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tblDialog">
						<c:forEach items="${categories}" var="category">
							<tr>
								<td>${category.cate_id}</td>
								<td>${category.cate_name}</td>
								<td><button data-toggle="modal" data-target="#myModal"
										class="btn btn-primary" type="button"
										onclick="update(this, ${category.cate_id})"
										value="${category.cate_name}">Update</button>
									<button class="btn btn-danger" type="button"
										onclick="deleteCategory(this, ${category.cate_id})"
										value="${count}">Delete</button></td>
							</tr>
							<c:set var="count" value="${count+1}"></c:set>
						</c:forEach>
					</tbody>
				</table>
			</section>
			<script type="text/javascript">
			function deleteCategory(button, cate_id){
				$.ajax({
					url : "./category/" + cate_id,
					type : "DELETE",
					data : "",
					contentType : "application/json",
					success : function(){
						var table = document.getElementById("categories-table");
						table.deleteRow(button.value);
						//
						var tr = table.getElementsByTagName("tr");
						for (var i = 1; i < tr.length; i++){
							tr[i].getElementsByTagName("button")[1].value = i;
						}
					}
				});
			}
			function update(button, cate_id){
				document.getElementById("btnSaveInModal").value = "update-"+cate_id;
				console.log(document.getElementById("btnSaveInModal").value);
				document.getElementById("txtCateName").value = button.value;
			}
			</script>
		</div>
	</div>
</body>
</html>