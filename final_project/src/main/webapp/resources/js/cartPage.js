//function deleteItem(pro_id, order) {
//	var url = "../cartController/item?proid=" + pro_id;
//	$.ajax({
//		url : url,
//		type : 'POST',
//		data : '',
//		contentType : 'application/json',
//		success : function(data) {
//			alert(1);
//		},
//		error : function(request, msg, error) {
//		}
//	});
//}
function changeWhenDescrease(pro_id, order) {
	var url = "../cartController/item?proid=" + pro_id + "&task=" + "descrease";
	$
			.ajax({
				url : url,
				type : 'POST',
				data : '',
				contentType : 'application/json',
				success : function(data) {
					var table = document.getElementById("cart-table");
					var tr = table.getElementsByTagName("tr");
					if (parseInt(tr[order].getElementsByTagName("input")[0].value) == 1)
						table.deleteRow(order);
					else {
						tr[order].getElementsByTagName("p")[0].innerHTML = parseInt(tr[order]
								.getElementsByTagName("p")[0].innerHTML)
								- Math
										.floor(parseInt(tr[order]
												.getElementsByTagName("p")[0].innerHTML)
												/ parseInt(tr[order]
														.getElementsByTagName("input")[0].value));
						tr[order].getElementsByTagName("input")[0].value = parseInt(tr[order]
								.getElementsByTagName("input")[0].value) - 1;
						if (parseInt(document
								.getElementById("num-item-in-cart").innerHTML) != 0)
							document.getElementById("num-item-in-cart").innerHTML = parseInt(document
									.getElementById("num-item-in-cart").innerHTML) - 1;
					}

					// Reset Total Price
					var total = 0;
					for (var i = 0; i < tr.length; i++)
						total += parseInt(tr[i].getElementsByTagName("p")[0].innerHTML);
					document.getElementById("total-price").innerHTML = total;

					initializeCheckOutContainer();
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
}

function getContextRootPath() {
	var pathName = location.pathname.substring(0);
	var token = pathName.split("/");
	var rootContextPath = "/" + token[0] + "/";
	return pathName;
}

function changeWhenInscrease(pro_id, order) {
	var url = "/item?proid=" + pro_id + "&task=" + "inscrease";
	console.log(getContextRootPath().replace("/cartPage", url));
	$
			.ajax({
				url : "/final_project/cartController/item",
				method : "POST",
				data : {
					proid : pro_id,
					task : "inscrease"
				},
				success : function(data) {
					var table = document.getElementById("cart-table");
					var tr = table.getElementsByTagName("tr");
					// Inscrease price
					tr[order].getElementsByTagName("p")[0].innerHTML = parseInt(tr[order]
							.getElementsByTagName("p")[0].innerHTML)
							+ Math
									.floor(parseInt(tr[order]
											.getElementsByTagName("p")[0].innerHTML)
											/ parseInt(tr[order]
													.getElementsByTagName("input")[0].value));
					// Incease quantity
					tr[order].getElementsByTagName("input")[0].value = parseInt(tr[order]
							.getElementsByTagName("input")[0].value) + 1;
					// Inscrease in cart button
					document.getElementById("num-item-in-cart").innerHTML = parseInt(document
							.getElementById("num-item-in-cart").innerHTML) + 1;

					// Reset Total Price
					var total = 0;
					for (var i = 0; i < tr.length; i++)
						total += parseInt(tr[i].getElementsByTagName("p")[0].innerHTML);
					document.getElementById("total-price").innerHTML = total;

					initializeCheckOutContainer();
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});

}

function implementDeleteItem(pro_id, order, button) {
	document.getElementById("agreeRemoveButton").value = "../cartController/item?proid="
			+ pro_id + "&task=delete" + "|" + button.value;
	document.getElementById("open-alert-remove-button").click();
}

function agreeRemove(yes_button) {
	var table = document.getElementById("cart-table");
	var tr = table.getElementsByTagName("tr");
	var deleteButton;
	$
			.ajax({
				url : yes_button.value.split("|")[0],
				type : 'DELETE',
				data : '',
				contentType : 'application/json',
				success : function() {
					// Decrease in cart button
					console.log(yes_button.value.split("|")[1]);
					document.getElementById("num-item-in-cart").innerHTML = parseInt(document
							.getElementById("num-item-in-cart").innerHTML)
							- parseInt(tr[yes_button.value.split("|")[1]]
									.getElementsByTagName("input")[0].value);

					// Remove this item from table
					table.deleteRow(yes_button.value.split("|")[1]);
					table = document.getElementById("cart-table");
					tr = table.getElementsByTagName("tr")
					for (var i = 0; i < tr.length; i++) {
						tr[i].getElementsByTagName("button")[2].value = i;
					}

					initializeCheckOutContainer();
				}
			});

}

function initializeCheckOutContainer() {
	var total = 0;
	var table = document.getElementById("cart-table");
	var tr = table.getElementsByTagName("tr");
	for (var i = 0; i < tr.length; i++)
		total += parseInt(tr[i].getElementsByTagName("p")[0].innerHTML);
	if (total > 0) {
		document.getElementById("check-out-button").style.visibility = "visible";
		document.getElementById("total-container").style.visibility = "visible";
		document.getElementById("keep-shopping-button").style.visibility = "hidden";
		document.getElementById("empty-cart-inform").style.visibility = "hidden";
		document.getElementById("total-price").innerHTML = total;

	} else {
		document.getElementById("check-out-button").style.visibility = "hidden";
		document.getElementById("total-container").style.visibility = "hidden";
		document.getElementById("total-price").style.visibility = "hidden";
		document.getElementById("keep-shopping-button").style.visibility = "visible";
		document.getElementById("empty-cart-inform").style.visibility = "visible";
	}
}

function getCities() {
	$.ajax({
		url : "./cities",
		type : 'GET',
		data : '',
		contentType : 'application/json',
		success : function(data) {
			var selectTag = document.getElementById("city-select-tag");
			var option;
			var txt;
			for (var i = 0; i < data.length; i++) {
				option = document.createElement("option");
				option.innerHTML = data[i].name;
				option.value = data[i].id;
				option.onclick = function() {
					getDistricts(this);
				}
				selectTag.appendChild(option);
			}
		}
	});
}

function getDistricts(option) {
	console.log(option.value);
	$.ajax({
		url : "./districts/" + option.value,
		type : 'GET',
		data : '',
		contentType : 'application/json',
		success : function(data) {
			var selectTag = document.getElementById("district-select-tag");
			var option = selectTag.getElementsByTagName("option");
			var txt;

			// Delete all district after selecting another city
			for (var i = option.length - 1; i > 0; i--) {
				selectTag.remove(1)
			}

			for (var i = 0; i < data.length; i++) {
				option = document.createElement("option");
				option.innerHTML = data[i].name;
				option.value = data[i].id;
				selectTag.appendChild(option);
			}
		}
	});
}

// Cart summary
function getInfoCartForCheckOut() {
	var mainCart = document.getElementById("cart-table");
	var cartCheckOut = document.getElementById("info-cart-table");
	var trTag;
	var tdTag;
	var aTag;
	var inputTag;
	var p;
	var listProduct = "";
	var listNum = "";

	// Xoa het
	trTag = cartCheckOut.getElementsByTagName("tr");
	for (var i = trTag.length - 1; i >= 0; i--) {
		cartCheckOut.deleteRow(i);
	}
	//

	// Chen tieu de
	trTag = document.createElement("tr");
	tdTag = document.createElement("td");
	tdTag.colSpan = 2;
	p = document.createElement("p");
	p.innerHTML = "CART SUMMARY";
	p.style.fontSize = "18px";
	p.style.cssFloat = "left";
	var hr = document.createElement("hr");
	tdTag.appendChild(p);
	tdTag.style.height = "30px";
	tdTag.style.padding = "0 15px";
	tdTag.style.display = "flex";
	trTag.appendChild(tdTag);
	trTag.style.borderBottom = "1px solid #e4e4e4";

	cartCheckOut.appendChild(trTag);
	//

	//
	var tr = mainCart.getElementsByTagName("tr");
	/* console.log(tr.length); */
	for (var i = 0; i < tr.length; i++) {
		trTag = document.createElement("tr");
		tdTag = document.createElement("td");
		tdTag.innerHTML = tr[i].getElementsByTagName("a")[0].innerHTML + " x "
				+ tr[i].getElementsByTagName("input")[0].value;
		tdTag.style.cssFloat = "left";
		tdTag.style.padding = "0 15px";
		trTag.appendChild(tdTag);
		tdTag = document.createElement("td");
		tdTag.innerHTML = tr[i].getElementsByTagName("p")[0].innerHTML
				+ "<span>&#8363</span>";
		/* tdTag.style.cssFloat = "right"; */
		tdTag.style.padding = "0 15px";
		trTag.appendChild(tdTag);
		trTag.style.borderBottom = "1px solid #e4e4e4";
		trTag.style.height = "30px";
		cartCheckOut.appendChild(trTag);
		// ../viewProductController/product?proid=3, lay ra list proid cho viec
		// xuat danh sach khi thanh toan va xoa ra khoi cart
		listProduct += tr[i].getElementsByTagName("a")[0].href.split("?")[1]
				.split("=")[1]+"."+ tr[i].getElementsByTagName("input")[0].value
				+ ",";
		listNum += (tr[i].getElementsByTagName("input")[0].value + ",");
	}// total-price
	trTag = document.createElement("tr");
	tdTag = document.createElement("td");
	tdTag.innerHTML = "Total : ";
	tdTag.style.fontSize = "20px";
	tdTag.style.fontWeight = "bold";
	tdTag.style.cssFloat = "left";
	tdTag.style.padding = "0 15px";
	trTag.appendChild(tdTag);
	tdTag = document.createElement("td");
	tdTag.style.fontSize = "19px";
	tdTag.innerHTML = document.getElementById("total-price").innerHTML
			+ "<span>&#8363</span>";
	tdTag.style.padding = "0 15px";
	trTag.appendChild(tdTag);
	cartCheckOut.appendChild(trTag);

	document.getElementById("buy-button").value = listProduct;
	console.log(document.getElementById("buy-button").value);
}

function buy(buy_button) {
	var fname = document.getElementById("txtFName").value;
	var lname = document.getElementById("txtLName").value;
	var email = document.getElementById("txtEmail").value;
	var phone = document.getElementById("txtPhone").value;
	var city = document.getElementById("city-select-tag").value;
	var district = document.getElementById("district-select-tag").value;

	var data = fname + "," + lname + "," + email + "," + phone + "," + city
			+ "," + district + ","
			+ document.getElementById("total-price").innerHTML + "-"
			+ buy_button.value;
	/* console.log(data); */

	$.ajax({
		url : "./bill?data=" + data,
		type : "POST",
		data : "",
		contentType : 'application/json',
		success : function(data) {
			console.log(data.join(","));
			writeBillDetail(data.join(","));
		},
		async : false,
		error : function(e) {
			console.log(e);
		}
	});
}

function writeBillDetail(data) {
	$
			.ajax({
				url : "./billdetail?data=" + data,
				type : "POST",
				data : "",
				contentType : 'application/json',
				success : function(data) {
					document.getElementById("form-checkout").style.display = "none";
					document.getElementById("buy-button").style.display = "none";
					document.getElementById("form-notify").style.display = "flex";
					document.getElementById("close-button").style.display = "block";

					var tableNotidfy = document.getElementById("table-notify");
					var tableCartSummary = document
							.getElementById("info-cart-table");
					var trTag;
					var span;

					// set address
					document.getElementById("shippingAddress").innerHTML = data.address;
					document.getElementById("name").innerHTML = data.name;
					document.getElementById("phone").innerHTML = data.phone;
					document.getElementById("email").innerHTML = data.email;
					//

					// set summary
					// Delete the last row in cart summary table (total)
					// Insert whole this table in the table-notify
					var tr = document.createElement("tr");
					var td = document.createElement("td");
					td.innerHTML = "Summary : ";
					tr.appendChild(td);
					td = document.createElement("td");
					td.style.cssFloat = "right";

					trTag = tableCartSummary.getElementsByTagName("tr");
					tableCartSummary.deleteRow(0);
					/* tableCartSummary.deleteRow(trTag.length - 1); */
					//
					for (var i = 0; i < trTag.length - 1; i++) {
						span = document.createElement("p");
						span.innerHTML = trTag[i].getElementsByTagName("td")[0].innerHTML;
						td.appendChild(span);

						tr.appendChild(td);
					}
					tableNotidfy.appendChild(tr);

					tr = document.createElement("tr");
					td = document.createElement("td");
					td.innerHTML = "Total : ";
					tr.appendChild(td);
					td = document.createElement("td");
					td.innerHTML = trTag[trTag.length - 1]
							.getElementsByTagName("td")[1].innerHTML;
					td.style.cssFloat = "right";
					tr.appendChild(td);
					tableNotidfy.appendChild(tr);
				},
				async : false,
				error : function(e) {
					console.log(e);
				}
			});
}