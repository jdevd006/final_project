//Handle images of product
$(document).ready(function() {
	$("#first-image").click(function() {
		$(this).css('border', '1px solid blue');
		$("#second-image").css('border', '1px solid #e6e6e6');
		$("#third-image").css('border', '1px solid #e6e6e6');
		getImageSource(this);
	});
	$("#second-image").click(function() {
		$(this).css('border', '1px solid blue');
		$("#first-image").css('border', '1px solid #e6e6e6');
		$("#third-image").css('border', '1px solid #e6e6e6');
		getImageSource(this);
	});
	$("#third-image").click(function() {
		$(this).css('border', '1px solid blue');
		$("#first-image").css('border', '1px solid #e6e6e6');
		$("#second-image").css('border', '1px solid #e6e6e6');
		getImageSource(this);
	});
});

function getImageSource(obj) {
	var divId = obj.id;
	var div = document.getElementById(divId);
	var img = div.getElementsByTagName("img");
	console.log(img[0].getAttribute("src"));
	showOnMainImage(img[0].getAttribute("src"));
}

function showOnMainImage(src) {
	var div = document.getElementById("main-image");
	var img = div.getElementsByTagName("img");
	img[0].style.width = "260px";
	img[0].style.height = "360px";
	img[0].src = src;
}

// Increase and decrease number of product
$(document).ready(
		function() {
			$("#btn-inscrease").click(
					function() {
						$("#quantity-field").val(
								(parseInt($("#quantity-field").val()) + 1));
					});
			$("#btn-descrease")
					.click(
							function() {
								if (parseInt($("#quantity-field").val()) > 1)
									$("#quantity-field").val(
											(parseInt($("#quantity-field")
													.val()) - 1));
							});
		});

// Buying function
/*
 * $(document) .ready( function() { $("#btnBuy") .click( function() {
 * $("#num-item-in-cart") .text( (parseInt($( "#num-item-in-cart") .text()) +
 * (parseInt($( "#quantity-field") .val())))); }); });
 */

// Review area
$(document).ready(function() {
	$("#open-review-field-button").click(function() {
		$("#before-press-review-button").css('display', 'none');
		$("#after-press-review-button").css('display', 'flex');
		$("#after-press-review-button").show();
	});

	$("#close-review-field-button").click(function() {
		$("#after-press-review-button").hide();
		$("#before-press-review-button").show();
	});

	// Call ajax
	$("#send-review-button").click(function() {

	});
});

function changeStarColorReviewOver(td) {
	var btnRemoveStars = document.getElementById("btnRemoveStars");
	var stars = document.getElementById("table-star-in-review");
	var star = stars.getElementsByTagName("td");

	for (var i = 0; i <= td.cellIndex; i++) {
		star[i].innerHTML = "<i class='fas fa-star'></i>";
		star[i].style.color = "yellow";
	}

	if (btnRemoveStars.value != "") {
		for (var i = td.cellIndex + 1; i < star.length; i++) {
			star[i].innerHTML = "<i class='far fa-star'></i>";
			star[i].style.color = "black";
		}
	}
}

function changeStarColorReviewOut(td) {
	var btnRemoveStars = document.getElementById("btnRemoveStars");
	var stars = document.getElementById("table-star-in-review");
	var star = stars.getElementsByTagName("td");
	if (btnRemoveStars.value == "") {
		for (var i = 0; i <= td.cellIndex; i++) {
			star[i].innerHTML = "<i class='far fa-star'></i>";
			star[i].style.color = "black";
		}
	} else {
		for (var i = 0; i <= btnRemoveStars.value; i++) {
			star[i].innerHTML = "<i class='fas fa-star'></i>";
			star[i].style.color = "yellow";
		}
		for (var i = parseInt(btnRemoveStars.value) + 1; i < star.length; i++) {
			star[i].innerHTML = "<i class='far fa-star'></i>";
			star[i].style.color = "black";
		}
	}
}

function clickOnStars(td) {
	var btnRemoveStars = document.getElementById("btnRemoveStars");
	btnRemoveStars.value = td.cellIndex;
	btnRemoveStars.style.display = "block";
	var stars = document.getElementById("table-star-in-review");
	var star = stars.getElementsByTagName("td");

	td.innerHTML = "<i class='fas fa-star'></i>";
	td.style.color = "yellow";
	for (var i = 0; i < td.cellIndex; i++) {
		star[i].innerHTML = "<i class='fas fa-star'></i>";
		star[i].style.color = "yellow";
	}
	for (var i = td.cellIndex + 1; i < star.length; i++) {
		star[i].innerHTML = "<i class='far fa-star'></i>";
		star[i].style.color = "black";
	}
}

function removeStars() {
	var stars = document.getElementById("table-star-in-review");
	var star = stars.getElementsByTagName("td");

	for (var i = 0; i < star.length; i++) {
		star[i].innerHTML = "<i class='far fa-star'></i>";
		star[i].style.color = "black";
	}
	var btnRemoveStars = document.getElementById("btnRemoveStars");
	btnRemoveStars.value = "";
	btnRemoveStars.style.display = "none";
}

function changeStarColorViewProduct(num) {
	var stars = document.getElementById("table-star");
	var star = stars.getElementsByTagName("td");
	for (var i = 0; i < num; i++) {
		star[i].innerHTML = "<i class='fas fa-star'></i>";
		star[i].style.color = "yellow";
	}
	stars = document.getElementById("table-star-review");
	star = stars.getElementsByTagName("td");
	for (var i = 0; i < num; i++) {
		star[i].innerHTML = "<i class='fas fa-star'></i>";
		star[i].style.color = "yellow";
	}
}

function goInToCart() {
	location.href = "./cartPage";
}

function addCart(id, isNullUser) {
	if (isNullUser == 0){
		sendAddRequest(
				"./addCart?id=" + id,
				function() {
					document.getElementById("num-item-in-cart").innerHTML = parseInt(document
							.getElementById("num-item-in-cart").innerHTML) + 1;
				});
	}else {
		/*document.cookie = "name=michel1, userid = 1; expires=Thu, 22 Dec 2020 19:55:20 UTC";
		alert(document.cookie);*/
		sendAddRequest(
				"./addCart?id=" + id,
				function() {
					document.getElementById("num-item-in-cart").innerHTML = parseInt(document
							.getElementById("num-item-in-cart").innerHTML) + 1;
				});
	}
}

function sendAddRequest(url, callback) {
	sendRequest(url, "", "POST", callback);
}

function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		data : JSON.stringify(data),
		type : method,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
		}
	});
}

function addReview(pro_id, user_id) {
	var star = document.getElementById("btnRemoveStars");
	var txtReview = document.getElementById("write-review-textbox");
	var content = txtReview.value;
	if (star != "")
		star = parseInt(star.value);
	else
		star = 0;
	console.log(pro_id)
	sendReviewRequest("./review?proid=" + pro_id + "&userid=" + user_id
			+ "&star=" + star + "&content=" + content, removeStars());
}

function sendReviewRequest(url, callback) {
	sendRequest(url, "", "POST", callback);
}

function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		data : JSON.stringify(data),
		type : method,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
		}
	});
}

function addQuestion(pro_id) {
	var content = document.getElementById("txtQuestion").value;
	sendQuestionRequest("./question?proid=" + pro_id + "&content=" + content,
			function() {
				document.getElementById("txtQuestion").value = "";
			});
}

function sendQuestionRequest(url, callback) {
	sendRequest(url, "", "POST", callback);
}

function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		data : JSON.stringify(data),
		type : method,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
		}
	});
}

function setNumItemCart(num) {
	document.getElementById("num-item-in-cart").innerHTML = num;
}

// Viewed items
$(document).ready(function() {
	$("#btnNext").click(function() {
		$("#btnPre").val(parseInt($("#btnPre").val()) + 1);
	});
	$("#btnPre").click(function() {
		$("#btnPre").val(parseInt($("#btnPre").val()) + 1);
	});
});

function getStartNum() {
	console.log(document.getElementById("btnPre").value);
	return parseInt(document.getElementById("btnPre").value);
}

var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
	showDivs(slideIndex += n);
}

function showDivs(n) {
	var i;
	var x = document.getElementsByClassName("mySlides");
	if (n > x.length) {
		slideIndex = 1
	}
	if (n < 1) {
		slideIndex = x.length
	}
	;
	for (i = 0; i < x.length; i++) {
		x[i].style.display = "none";
	}
	x[slideIndex - 1].style.display = "block";
}

function separatePageViewed(numViewedItem) {
	var mainPages = Math.floor(numViewedItem / 5);
	var subPages = numViewedItem % 5;
	var linksContainer = document.getElementById("separate-links");
	var li;
	var a;
	li = document.createElement("li");
	li.className = "page-item";
	a = document.createElement("button");
	a.className = "page-link";
	a.innerHTML = "Previous";
	a.value = 1;
	a.id = "leftward-button";
	a.onclick = function() {
		Comeback(this)
	};
	li.appendChild(a);
	linksContainer.appendChild(li);
	if (mainPages != 0) {
		for (var i = 0; i < mainPages; i++) {
			li = document.createElement("li");
			li.className = "page-item";
			a = document.createElement("button");
			a.className = "page-link";
			a.innerHTML = i + 1;
			a.onclick = function() {
				movePage(this)
			};
			li.appendChild(a);
			linksContainer.appendChild(li);
		}
	}
	if (subPages != 0) {
		li = document.createElement("li");
		li.className = "page-item";
		a = document.createElement("button");
		a.className = "page-link";
		a.innerHTML = mainPages + 1;
		a.onclick = function() {
			movePage(this)
		};
		li.appendChild(a);
		linksContainer.appendChild(li);
	}
	li = document.createElement("li");
	li.className = "page-item";
	a = document.createElement("button");
	a.className = "page-link";
	a.innerHTML = "Next";
	a.value = 1;
	a.id = "rightward-button";
	a.onclick = function() {
		Cometo()
	};
	li.appendChild(a);
	linksContainer.appendChild(li);
}

function movePage(aTag) {
	var table = document.getElementById("table-viewed-item");
	var tr = table.getElementsByTagName("tr");
	for (var i = 0; i < tr.length; i++) {
		if (i == parseInt(aTag.innerHTML) - 1) {
			tr[i].style.display = "block";
			document.getElementById("leftward-button").value = i + 1;
			document.getElementById("rightward-button").value = i + 1;
		} else {
			tr[i].style.display = "none";
		}
	}

}

function Comeback() {
	var currentPage = parseInt(document.getElementById("leftward-button").value);
	/* alert(currentPage); */
	if (currentPage > 1) {
		var table = document.getElementById("table-viewed-item");
		var tr = table.getElementsByTagName("tr");
		for (var i = 0; i < tr.length; i++) {
			if (i == currentPage - 2) {
				tr[i].style.display = "block";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
	if (currentPage == 1) {
		document.getElementById("leftward-button").value = currentPage;
		document.getElementById("rightward-button").value = currentPage;
	} else {
		document.getElementById("leftward-button").value = currentPage - 1;
		document.getElementById("rightward-button").value = currentPage - 1;
	}
}

function Cometo() {
	var linksContainer = document.getElementById("separate-links");
	var links = linksContainer.getElementsByTagName("li");
	var currentPage = parseInt(document.getElementById("rightward-button").value);
	/* alert(currentPage); */
	if (currentPage >= 1 && currentPage < links.length - 2) {
		var table = document.getElementById("table-viewed-item");
		var tr = table.getElementsByTagName("tr");
		for (var i = 0; i < tr.length; i++) {
			if (i == (currentPage + 1) - 1) {
				tr[i].style.display = "block";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
	if (currentPage == links.length - 2) {
		document.getElementById("leftward-button").value = currentPage;
		document.getElementById("rightward-button").value = currentPage;
	} else {
		document.getElementById("leftward-button").value = currentPage + 1;
		document.getElementById("rightward-button").value = currentPage + 1;
	}
}
