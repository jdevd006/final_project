package com.jdev006.entitties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "message")
public class Message {
	private int id;
	private String quesion;
	private String answer;
	
	private Account_detail account_detail;

	public Message() {
	}

	public Message(int id, String quesion, String answer) {
		this.id = id;
		this.quesion = quesion;
		this.answer = answer;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "question", nullable = false)
	public String getQuesion() {
		return quesion;
	}

	public void setQuesion(String quesion) {
		this.quesion = quesion;
	}

	@Column(name = "answer", nullable = false)
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@OneToOne
	@JoinColumn(name = "user_id")
	public Account_detail getAccount_detail() {
		return account_detail;
	}

	public void setAccount_detail(Account_detail account_detail) {
		this.account_detail = account_detail;
	}
	
}
