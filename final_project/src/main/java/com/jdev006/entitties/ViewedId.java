package com.jdev006.entitties;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ViewedId implements Serializable{
	@Column(name = "user_id")
	private int account_detail;
	@Column(name = "pro_id")
	private int product;

	public ViewedId() {
	}

	public ViewedId(int account_detail, int product) {
		this.account_detail = account_detail;
		this.product = product;
	}

	public int getAccount_detail() {
		return account_detail;
	}

	public void setAccount_detail(int account_detail) {
		this.account_detail = account_detail;
	}

	public int getProduct() {
		return product;
	}

	public void setProduct(int product) {
		this.product = product;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ViewedId)) return false;
        ViewedId that = (ViewedId) o;
        return Objects.equals(getAccount_detail(), that.getAccount_detail()) &&
                Objects.equals(getProduct(), that.getProduct());
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(getAccount_detail(), getProduct());
    }
}
