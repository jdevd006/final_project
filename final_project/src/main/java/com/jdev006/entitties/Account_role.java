package com.jdev006.entitties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account_role", catalog = "web1")
public class Account_role {
	private String role;
	private int id;
	
	/* @OneToOne(mappedBy = "account_role", cascade = CascadeType.ALL) */
	private Account_detail account_detail; 

	public Account_role() {
	}

	public Account_role(Account_detail account_detail, String role) {
		/* this.account_detail = account_detail; */
		this.role = role;
	}
	
	/*
	 * public Account_detail getAccount_detail() { return account_detail; }
	 * 
	 * public void setUser_id(Account_detail account_detail) { this.account_detail =
	 * account_detail; }
	 */
	
	@Column(name = "role")
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "userId", referencedColumnName = "user_id")
	public Account_detail getAccount_detail() {
		return account_detail;
	}

	public void setAccount_detail(Account_detail account_detail) {
		this.account_detail = account_detail;
	}
	
	
	
}
