package com.jdev006.entitties;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BillDetailId implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "bill_id")
	private int bill_id;
	@Column(name = "pro_id")
	private int pro_id;

	public BillDetailId() {
	}

	public BillDetailId(int bill_id, int pro_id) {
		this.pro_id = pro_id;
		this.bill_id = bill_id;
	}

	public int getpro_id() {
		return pro_id;
	}

	public void setPro_id(int pro_id) {
		this.pro_id = pro_id;
	}

	public int getBill_id() {
		return bill_id;
	}

	public void setBill_id(int bill_id) {
		this.bill_id = bill_id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof CartId))
			return false;
		CartId that = (CartId) o;
		return Objects.equals(getpro_id(), that.getpro_id()) && Objects.equals(getBill_id(), that.getUser_id());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getpro_id(), getBill_id());
	}
}
