package com.jdev006.entitties;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "billdetail", catalog = "web1")
public class BillDetail {
	@EmbeddedId
	private BillDetailId id;
	
	private int num;
	
	public BillDetail() {
	}

	public BillDetailId getId() {
		return id;
	}

	public void BillDetailId(BillDetailId id) {
		this.id = id;
	}

	public void setId(BillDetailId id) {
		this.id = id;
	}

	@Column(name = "num")
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
}
