package com.jdev006.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Account_role;
import com.jdev006.entitties.TripleDES;
import com.jdev006.service.Account_detail_service;
import com.jdev006.service.Account_role_service;

@Controller
@RequestMapping(value = "/loginRegisterController")
@SessionAttributes({ "currentUser", "role" })
public class Login_RegisterController {
	@Autowired
	private Account_detail_service account_detaiService;
	@Autowired
	private Account_role_service account_roleService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView motoLoginPage(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		model.setViewName("regist_form");
		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(@RequestParam("email") String email, @RequestParam("password") String password,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Account_detail account = null;
		for (Account_detail acc : account_detaiService.getAll()) {
			if (acc.getEmail().equalsIgnoreCase(email)) {
				account = acc;
				break;
			}
		}
		if (account != null) {
			if (password.equalsIgnoreCase(TripleDES.Decrypt(account.getPassword(), "12345"))) {
				model.addObject("currentUser", account);
				model.addObject("role", account_roleService.get(account.getUser_id()).getRole());
				System.out.println(account_roleService.get(account.getUser_id()).getRole());
				model.setViewName("redirect:/cartController/cartPage");
			} else {
				model.setViewName("redirect:/loginRegisterController/login");
			}
		} else {
			model.setViewName("redirect:/loginRegisterController/login");
		}
		return model;
	}

	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	public ModelAndView register(@RequestParam("email") String email, @RequestParam("psw") String password,
			@RequestParam("username") String username, @RequestParam("phone") String phone) {
		ModelAndView model = new ModelAndView();
		model.setViewName("redirect:/loginRegisterController/login");
		Account_detail account = new Account_detail();
		Account_role accoutRole = new Account_role();
		account.setEmail(email);
		account.setUser_name(username);
		account.setPhone(phone);
		account.setPassword(TripleDES.Encrypt(password, "12345"));
		// set default role : user
		accoutRole.setRole("user");
		accoutRole.setAccount_detail(account);
		//
		account.setAccount_role(accoutRole);
		account_detaiService.add(account);
		model.addObject("txt", account.getPassword());
		return model;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(SessionStatus sessionStatus) {
		ModelAndView model = new ModelAndView();
		sessionStatus.setComplete();
		model.setViewName("redirect:/cartController/cartPage");
		return model;
	}
}
