package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.text.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Cart;
import com.jdev006.entitties.CartId;
import com.jdev006.entitties.Image;
import com.jdev006.entitties.Item;
import com.jdev006.entitties.Product;
import com.jdev006.entitties.Question_Answer;
import com.jdev006.entitties.Review_Product;
import com.jdev006.entitties.Viewed;
import com.jdev006.service.Account_detail_service;
import com.jdev006.service.Cart_services;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;
import com.jdev006.service.Question_Answer_service;
import com.jdev006.service.Review_Product_service;
import com.jdev006.service.Viewed_Services;

@Controller
@RequestMapping(value = "/viewProductController")
public class ViewProductController {
	@Autowired
	private Product_service productServices;
	@Autowired
	private Cart_services cartServices;
	@Autowired
	private Question_Answer_service qaServices;
	@Autowired
	private Viewed_Services viewedServices;
	@Autowired
	private Review_Product_service reviewServices;
	@Autowired
	private Account_detail_service accountServices;
	@Autowired
	private Image_service imgServices;

	@RequestMapping(value = "/product", method = RequestMethod.GET)
	private ModelAndView test(@RequestParam("proid") int pro_id, HttpSession httpSession) {
		ModelAndView model = new ModelAndView();
		int numItem = 0;
		ArrayList<Product> listViewed = new ArrayList<Product>();
		ArrayList<Question_Answer> listAQ = new ArrayList<Question_Answer>();
		ArrayList<Review_Product> listReview = new ArrayList<Review_Product>();
		ArrayList<Image> listImage = new ArrayList<Image>();
		int count = 0;
		if (httpSession.getAttribute("currentUser") != null) {
			Account_detail acc = (Account_detail) httpSession.getAttribute("currentUser");
			System.out.println("sdsadadfasdad" + acc.getEmail());
			for (Cart cart : cartServices.getAll()) {
				if (cart.getId().getUser_id() == acc.getUser_id())
					numItem += cart.getNum();
			}

			for (Viewed viewed : viewedServices.getAll()) {
				if (viewed.getId().getAccount_detail() == acc.getUser_id())
					listViewed.add(productServices.get(viewed.getId().getProduct()));
			}

		}

		// Yêu cầu iid của sản phẩm
		for (Question_Answer qa : qaServices.getAll()) {
			if (qa.getProduct().getPro_id() == pro_id)
				listAQ.add(qa);
		}

		for (Review_Product review : reviewServices.getAll()) {
			count++;
			if (count == 6)
				break;
			listReview.add(review);
		}
		count = 0;
		for (Image img : imgServices.getAll()) {
			if (count == 4)
				break;
			if (img.getProduct().getPro_id() == pro_id) {
				listImage.add(img);
			}

		}
		model.setViewName("viewProduct");
		model.addObject("cartItems", numItem);
		model.addObject("listQA", listAQ);
		model.addObject("listViewed", listViewed);
		model.addObject("listReview", listReview);
		model.addObject("listImage", listImage);
		model.addObject("product", productServices.get(pro_id));
		model.addObject("listImage", listImage);
		return model;
	}

	@RequestMapping(value = "/cartPage", method = RequestMethod.GET)
	public String cart(ModelMap model) {
		return "redirect:/cartController/cartPage";
	}

	@RequestMapping(value = "/addCart", method = RequestMethod.POST)
	@ResponseBody
	public void addToCart(@RequestParam int id, HttpSession httpSession) {
		Cart cart = null;
		CartId cartId = null;
		Account_detail acc = (Account_detail) httpSession.getAttribute("currentUser");
		boolean check = false;
		if (acc != null) {
			for (Cart c : cartServices.getAll()) {
				if (c.getId().getpro_id() == id && c.getId().getUser_id() == acc.getUser_id()) {
					c.setNum(c.getNum() + 1);
					cartServices.update(c);
					cart = c;
					check = true;
					break;
				}
			}
			if (!check) {
				cart = new Cart();
				cartId = new CartId();
				cartId.setPro_id(id);
				cartId.setUser_id(acc.getUser_id());
				cart.setId(cartId);
				cart.setNum(1);
				cartServices.add(cart);
			}
		}
		if (cart == null) {
			for (Cart c : cartServices.getAll()) {
				if (c.getId().getpro_id() == id && c.getId().getUser_id() == 0) {
					c.setNum(c.getNum() + 1);
					cartServices.update(c);
					check = true;
					break;
				}
			}
			if (!check) {
				cart = new Cart();
				cartId = new CartId();
				cartId.setPro_id(id);
				cartId.setUser_id(0);
				cart.setId(cartId);
				cart.setNum(1);
				cartServices.add(cart);
			}

			/*
			 * cart = new Cart(); CartId cartId = new CartId(); cartId.setPro_id(id);
			 * cartId.setUser_id(0); cart.setId(cartId); cart.setNum(1);
			 * cartServices.add(cart);
			 */
		}
	}

	@RequestMapping(value = "/review", method = RequestMethod.POST)
	@ResponseBody
	public void review(@RequestParam("userid") int user_id, @RequestParam("star") int star,
			@RequestParam("content") String content, @RequestParam("proid") int pro_id) {
		Review_Product review = new Review_Product();
		review.setPro_star(star);
		review.setContent(content);
		review.setAccount_detail(accountServices.get(user_id));
		review.setProduct(productServices.get(pro_id));
		reviewServices.add(review);
	}

	@RequestMapping(value = "/question", method = RequestMethod.POST)
	@ResponseBody
	public void quesion(@RequestParam("proid") int pro_id, @RequestParam("content") String q_content) {
		Question_Answer qa = new Question_Answer();
		qa.setProduct(productServices.get(pro_id));
		qa.setQ_content(q_content);
		qa.setA_content("");
		qaServices.add(qa);
	}

	@RequestMapping(value = "/movetologin", method = RequestMethod.GET)
	public String login() {
		return "redirect:/loginRegisterController/login";
	}
}
