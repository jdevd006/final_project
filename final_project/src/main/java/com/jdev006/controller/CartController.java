package com.jdev006.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.omg.PortableServer.POA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Bill;
import com.jdev006.entitties.BillDetail;
import com.jdev006.entitties.BillDetailId;
import com.jdev006.entitties.Cart;
import com.jdev006.entitties.CartId;
import com.jdev006.entitties.City;
import com.jdev006.entitties.District;
import com.jdev006.entitties.Image;
import com.jdev006.entitties.Item;
import com.jdev006.service.Account_detail_service;
import com.jdev006.service.BillDetaiService;
import com.jdev006.service.Bill_service;
import com.jdev006.service.Cart_services;
import com.jdev006.service.City_services;
import com.jdev006.service.District_services;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;

@Controller
@RequestMapping(value = "/cartController")
public class CartController {
	@Autowired
	private Cart_services cartServices;
	@Autowired
	private Product_service productServices;
	@Autowired
	private Image_service imgServices;
	@Autowired
	private City_services cityServices;
	@Autowired
	private Account_detail_service accountServices;
	@Autowired
	private District_services districtServices;
	@Autowired
	private Bill_service billServices;
	@Autowired
	private BillDetaiService billDetailServices;

	@RequestMapping(value = "/cartPage", method = RequestMethod.GET)
	public String cartPage(ModelMap model, HttpSession httpSession) {
		int numItem = 0;
		ArrayList<Item> listItems = new ArrayList<Item>();
		ArrayList<Image> listImage = new ArrayList<Image>();
		ArrayList<Image> listAllImage = (ArrayList<Image>) imgServices.getAll();

		accountServices.getByEmail("lamthanh1451999@gmail.com");

		if (httpSession.getAttribute("currentUser") != null) {
			Account_detail acc = (Account_detail) httpSession.getAttribute("currentUser");
			for (Cart cart : cartServices.getAll()) {
				if (cart.getId().getUser_id() == acc.getUser_id()) {
					numItem += cart.getNum();
					listItems.add(new Item(productServices.get(cart.getId().getpro_id()), cart.getNum()));
				}
			}
			for (Item item : listItems) {
				for (Image img : listAllImage) {
					if (item.getProduct().getPro_id() == img.getProduct().getPro_id()) {
						listImage.add(img);
						break;
					}
				}
			}
		} else {
			for (Cart cart : cartServices.getAll()) {
				if (cart.getId().getUser_id() == 0) {
					numItem += cart.getNum();
					listItems.add(new Item(productServices.get(cart.getId().getpro_id()), cart.getNum()));
				}
			}
			for (Item item : listItems) {
				for (Image img : listAllImage) {
					if (item.getProduct().getPro_id() == img.getProduct().getPro_id()) {
						listImage.add(img);
						break;
					}
				}
			}
		}

		model.addAttribute("listItems", listItems);
		model.addAttribute("listImage", listImage);
		model.addAttribute("cartItems", numItem);
		return "cartPage";
	}

	@RequestMapping(value = "/item", method = RequestMethod.POST)
	@ResponseBody
	public void addItem(@RequestParam("proid") int pro_id, @RequestParam("task") String task) {
		if (task.equalsIgnoreCase("descrease")) {
			for (Cart c : cartServices.getAll()) {
				if (c.getId().getpro_id() == pro_id) {
					if (c.getNum() == 1) {
						cartServices.delete(c);
					} else {
						c.setNum(c.getNum() - 1);
						cartServices.update(c);
					}
				}
			}
		}
		if (task.equalsIgnoreCase("inscrease")) {
			for (Cart c : cartServices.getAll()) {
				if (c.getId().getpro_id() == pro_id) {
					c.setNum(c.getNum() + 1);
					cartServices.update(c);
				}
			}
		}
	}

	@RequestMapping(value = "/item", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteItem(@RequestParam("proid") int pro_id) {
		Cart cart = new Cart();
		CartId c = new CartId(pro_id, 3);
		cart.setId(c);
		cartServices.delete(cart);
	}

	@RequestMapping(value = "/cities", method = RequestMethod.GET)
	@ResponseBody
	public List<City> returnCity() {
		List<City> cities = new ArrayList<City>();
		cities = cityServices.getAll();
		cityServices.getAll2();
		return cities;
	}

	@RequestMapping(value = "/districts/{city_id}", method = RequestMethod.GET)
	@ResponseBody
	public List<District> returnDistrict(@PathVariable("city_id") int city_id) {
		List<District> districts = new ArrayList<District>();
		districts = districtServices.getListByCityId(city_id);
		System.out.println(city_id);
		return districts;
	}

	@RequestMapping(value = "/bill", method = RequestMethod.POST)
	@ResponseBody
	public String[] handlBuy(@RequestParam("data") String data, HttpSession httpSession) {
		// nguyen
		// lam,thanh,nlthanhititu17038@gamil.com,0964774964,1,1,750000-listProduct
		System.out.println("sdsdsdsdsdsdsdsd" + data);
		Account_detail account = null;
		String[] main = data.split("-");
		String[] details = main[0].split(",");
		String[] proIds = main[1].split(",");
		/*
		 * for (int i = 0; i < proIds.length; i++) { if (i != proIds.length - 1) { pros
		 * += (proIds[i] + ","); } else pros += proIds[i]; }
		 */
		Bill bill = new Bill();

		Calendar c = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = c.getTime();
		if (httpSession.getAttribute("currentUser") != null) {
			account = (Account_detail) httpSession.getAttribute("currentUser");
			bill.setTotal(Integer.parseInt(details[6]));
			bill.setAccount_detail(account);
			bill.setOrder_date(format.format(date));
			c.add(Calendar.DAY_OF_MONTH, 2);
			bill.setDue_date(format.format(c.getTime()));
			bill.setAddress(cityServices.get(Integer.parseInt(details[4])).getName() + "-"
					+ districtServices.get(Integer.parseInt(details[5])).getName());
			bill.setName(details[0] + " " + details[1]);
			bill.setPhone(details[3]);
			bill.setEmail(details[2]);
			billServices.add(bill);
		} else {
			bill.setTotal(Integer.parseInt(details[6]));
			bill.setAccount_detail(null);
			bill.setOrder_date(format.format(date));
			c.add(Calendar.DAY_OF_MONTH, 2);
			bill.setDue_date(format.format(c.getTime()));
			bill.setAddress(cityServices.get(Integer.parseInt(details[4])).getName() + "-"
					+ districtServices.get(Integer.parseInt(details[5])).getName());
			bill.setName(details[0] + " " + details[1]);
			bill.setPhone(details[3]);
			bill.setEmail(details[2]);
			billServices.add(bill);
			System.out.println("asdfisdgfisadgfiusdfiu");
		}
		for (String str : proIds) {
			System.out.println(str);
		}
		return proIds;
	}

	@RequestMapping(value = "/billdetail", method = RequestMethod.POST)
	@ResponseBody
	public Bill handleBillDetai(@RequestParam("data") String data, HttpSession httpSession) {
		Account_detail account = (Account_detail) httpSession.getAttribute("currentUser");
		BillDetail billDetail = new BillDetail();
		String[] proIds = data.split(",");
		BillDetailId billDetailId = new BillDetailId();
		billDetailId.setBill_id(billServices.getAll().get(billServices.getAll().size() - 1).getBill_id());
		System.out.println("sdfsdfsdfsdfs" + proIds[0].indexOf("."));
		if (httpSession.getAttribute("currentUser") != null) {
			for (int i = 0; i < proIds.length; i++) {
				billDetailId.setPro_id(Integer.parseInt(proIds[i].substring(0, proIds[0].indexOf("."))));
				billDetail.setNum(Integer.parseInt(proIds[i].substring(proIds[0].indexOf(".")+1)));
				billServices.deteleByProId(Integer.parseInt(proIds[i].substring(0, proIds[0].indexOf("."))), account.getUser_id());
				billDetail.setId(billDetailId);
				billDetailServices.add(billDetail);
			}
		} else {
			for (int i = 0; i < proIds.length; i++) {
				billDetailId.setPro_id(Integer.parseInt(proIds[i].substring(0, proIds[0].indexOf("."))));
				billServices.deteleByProId(Integer.parseInt(proIds[i].substring(0, proIds[0].indexOf("."))), 0);
				billDetail.setId(billDetailId);
				billDetailServices.add(billDetail);
			}
			System.out.println("bill detail");
		}
		Bill bill = new Bill();
		bill = billServices.getAll().get(billServices.getAll().size() - 1);
		return bill;
	}

	@RequestMapping(value = "/billdetailreturn", method = RequestMethod.GET)
	@ResponseBody
	public Account_detail getBillDetail(HttpSession httpSession) {
		Account_detail account = (Account_detail) httpSession.getAttribute("currentUser");
		Bill bill = new Bill();
		bill = billServices.getBillByUserId(account.getUser_id());
		List<Object> listObjects = new ArrayList<Object>();
		listObjects.add(bill);
		listObjects.add(account);
		return account;
	}
}
