package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jdev006.entitties.Category;
import com.jdev006.entitties.Product;
import com.jdev006.service.Category_service;
import com.jdev006.service.Product_service;

@Controller
@RequestMapping(value = "/productController")
public class ManageProductController {
	@Autowired
	private Product_service productService;
	@Autowired
	private Category_service categoryServices;

	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ModelAndView getAll(@ModelAttribute Product product) {
		ModelAndView model = new ModelAndView();
		model.setViewName("admin_Product");
		model.addObject("products", productService.getAll());
		model.addObject("categories", categoryServices.getAll());
		return model;
	}

	@RequestMapping(value = "/product", method = RequestMethod.POST)
	public String addProduct(@ModelAttribute Product product, @RequestParam String btnSaveInModal) {
		ModelAndView model = new ModelAndView();
		model.setViewName("admin_Product");
		if (btnSaveInModal.split("-")[0].equalsIgnoreCase("update")) {
			product.setPro_id(Integer.parseInt(btnSaveInModal.split("-")[1]));
			productService.update(product);
		} else {
			product.setPro_star(0);
			productService.add(product);
		}
		return "redirect:/productController/products";
	}

	@RequestMapping(value = "/product/{pro_id}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable(value = "pro_id") int pro_id) {
		System.out.println(pro_id);
		productService.deteleByProId(pro_id);
	}
	
	/*
	 * @RequestMapping(value = "/aproduct", method = RequestMethod.GET)
	 * 
	 * @ResponseBody public Product getAProduct(@RequestParam("proId") int proId) {
	 * Product product = new Product(); product = productService.getAProduct(proId);
	 * System.out.println(product.getCategory().getCate_id()); return product; }
	 */
	@RequestMapping(value = "/aproduct", method = RequestMethod.GET)
	@ResponseBody
	public List<Object> getAProduct(@RequestParam("proId") int proId) {
		Product product = new Product();
		Category cate = new Category();
		product = productService.getAProduct(proId);
		List<Object> list= new ArrayList<Object>();
		System.out.println(product.getCategory().getCate_name());
		list.add(product);
		list.add(categoryServices.get(product.getCategory().getCate_id()));
		return list;
	}
}
