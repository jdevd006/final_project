package com.jdev006.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Account_role;
import com.jdev006.service.Account_detail_service;
import com.jdev006.service.Account_role_service;

@Controller
@RequestMapping(value = "/userController")
public class UserControlller {
	@Autowired
	private Account_detail_service accountDetailServices;
	@Autowired
	private Account_role_service accountRoleServices;
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ModelAndView accounts(@ModelAttribute("user") Account_detail user) {
		ModelAndView model = new ModelAndView();
		model.setViewName("admin_Account");
		List<Account_role> roles = new  ArrayList<Account_role>();
		for (Account_detail account : accountDetailServices.getAll()) {
			roles.add(accountRoleServices.getByUserId(account.getUser_id()));
		}
		model.addObject("users", accountDetailServices.getAll());
		model.addObject("roles", roles);
		return model;
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	 public String addCategory(@ModelAttribute Account_detail user, @RequestParam String btnSaveInModal) {
		 if (btnSaveInModal.split("-")[0].equalsIgnoreCase("update")) {

		 }else {
		 }
		 return "redirect:/userController/users";
	 }
}
