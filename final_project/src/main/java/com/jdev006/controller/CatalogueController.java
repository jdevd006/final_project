package com.jdev006.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jdev006.entitties.Category;
import com.jdev006.service.Category_service;

@Controller
@RequestMapping(value = "/categoryController")
public class CatalogueController {
	 @Autowired
	 private Category_service categoryServices;
	 
	 @RequestMapping(value = "/categories", method = RequestMethod.GET)
	 public ModelAndView categoryPage(@ModelAttribute Category category) {
		 ModelAndView model = new ModelAndView();
		 model.setViewName("admin_Catalog");
		 model.addObject("categories", categoryServices.getAll());
		 return model;
	 }
	 
	 @RequestMapping(value = "/category", method = RequestMethod.POST)
	 public String addCategory(@ModelAttribute Category category, @RequestParam String btnSaveInModal) {
		 System.out.println("sdsdsd : " + btnSaveInModal.split("-")[0]);
		 if (btnSaveInModal.split("-")[0].equalsIgnoreCase("update")) {
			 category.setCate_id(Integer.parseInt(btnSaveInModal.split("-")[1]));
			 categoryServices.update(category);
		 }else {
			 categoryServices.add(category);
		 }
		 return "redirect:/categoryController/categories";
	 }
	 
	 @RequestMapping(value= "/category/{cate_id}", method=RequestMethod.DELETE)
	 public @ResponseBody void delete(@PathVariable() int cate_id) {
		categoryServices.delete(cate_id);
	 }
}
