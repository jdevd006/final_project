package com.jdev006.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.jdev006.service.Account_detail_service;
import com.jdev006.service.Cart_services;
import com.jdev006.service.Image_service;
import com.jdev006.service.Product_service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jdev006.entitties.Account_detail;
import com.jdev006.entitties.Image;
import com.jdev006.entitties.Product;

@Controller
@RequestMapping(value = "/homecontroller")
public class HomepageController {
	@Autowired
	private Product_service productService;
	@Autowired
	private Cart_services cartServices;
	@Autowired
	private Account_detail_service accountServices;
	@Autowired
	private Image_service imgServices;

	@RequestMapping(value = "/homepage", method =  RequestMethod.GET)
	private ModelAndView homepagetest( HttpSession httpSession) {
		ModelAndView model = new ModelAndView();
		model.setViewName("homePage");
		List<Product> listpro = productService.getAll();
		List<Image> listImg = new ArrayList<>();
	
		if (httpSession.getAttribute("currentUser") !=null) {
			Account_detail acc=(Account_detail) httpSession.getAttribute("currentUser");
		}
		
		for (Product pro : productService.getAll()) {
			for (Image img : imgServices.getAll()) {
				if (img.getProduct().getPro_id() == pro.getPro_id()) {
					listImg.add(img);
				
					break;
				}
			}
			
		}
		
		
		model.addObject("listimg", listImg);
		model.addObject("listpro", listpro);
		return model;
	}

}
