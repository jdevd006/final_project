package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.City;

@Transactional
@Service
public class City_services {
	@Autowired
	DAO<City> City_DAO;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<City> getAll(){
		return City_DAO.getAll();
	}
	
	public City get(int id){
		return City_DAO.get(id);
	}
	
	public City add(City t){
		return City_DAO.add(t);
	}
	
	public Boolean update(City t){
		return City_DAO.update(t);
	}
	
	public Boolean delete(City t){
		return City_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return City_DAO.delete(id);
	}
	
	public List<City> getAll2(){
		Session session = sessionFactory.getCurrentSession();
		List<City> list = new ArrayList<City>();
		String hql = "from City";
		Query query = session.createQuery(hql);
		list = query.list();
		System.out.println(list);
		return list;
	}
}
