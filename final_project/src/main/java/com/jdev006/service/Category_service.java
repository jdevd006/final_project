package com.jdev006.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Category;

@Transactional
@Service
public class Category_service {
	@Autowired
	DAO<Category> Category_DAO;
	@Autowired
	private SessionFactory sessionFactory;
	public List<Category> getAll(){
		return Category_DAO.getAll();
	}
	
	public Category get(int id){
		return Category_DAO.get(id);
	}
	
	public Category add(Category t){
		return Category_DAO.add(t);
	}
	
	public Boolean update(Category t){
		return Category_DAO.update(t);
	}
	
	public Boolean delete(Category t){
		return Category_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Category_DAO.delete(id);
	}
}
