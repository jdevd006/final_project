package com.jdev006.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Message;

@Transactional
@Service
public class Message_service {
	@Autowired
	DAO<Message> Message_DAO;
	
	public List<Message> getAll(){
		return Message_DAO.getAll();
	}
	
	public Message get(int id){
		return Message_DAO.get(id);
	}
	
	public Message add(Message t){
		return Message_DAO.add(t);
	}
	
	public Boolean update(Message t){
		return Message_DAO.update(t);
	}
	
	public Boolean delete(Message t){
		return Message_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Message_DAO.delete(id);
	}
}
