package com.jdev006.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.BillDetail;

@Transactional
@Service
public class BillDetaiService {
	@Autowired
	private DAO<BillDetail> billDetailDao;
	@Autowired
	private SessionFactory sessionFactory;
	public List<BillDetail> getAll(){
		return billDetailDao.getAll();
	}
	
	public BillDetail get(int id){
		return billDetailDao.get(id);
	}
	
	public BillDetail add(BillDetail t){
		return billDetailDao.add(t);
	}
	
	public Boolean update(BillDetail t){
		return billDetailDao.update(t);
	}
	
	public Boolean delete(BillDetail t){
		return billDetailDao.delete(t);
	}
	
	public Boolean delete(int id){
		return billDetailDao.delete(id);
	}
	
	public  List<BillDetail> getListByBillId(int billId){
		Session session = sessionFactory.getCurrentSession();
		List<BillDetail> billDetails = new ArrayList<BillDetail>();
		String hql = "from BillDetail where bill_id = :billId";
		Query query = session.createQuery(hql);
		query.setParameter("billId", billId);
		billDetails = query.list();
		return billDetails;
	}
}
