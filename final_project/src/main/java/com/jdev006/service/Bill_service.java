package com.jdev006.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Bill;

@Transactional
@Service
public class Bill_service {
	@Autowired
	DAO<Bill> Bill_DAO;
	@Autowired
	private SessionFactory sessionFactory;
	public List<Bill> getAll(){
		return Bill_DAO.getAll();
	}
	
	public Bill get(int id){
		return Bill_DAO.get(id);
	}
	
	public Bill add(Bill t){
		return Bill_DAO.add(t);
	}
	
	public Boolean update(Bill t){
		return Bill_DAO.update(t);
	}
	
	public Boolean delete(Bill t){
		return Bill_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Bill_DAO.delete(id);
	}
	
	public Bill getBillByUserId(int userId) {
		Bill bill = new Bill();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where user_id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		if (query.list().size() == 0) return null;
		return (Bill) query.list().get(query.list().size()-1);
	}
	
	public List<Bill> getListBillByUserId(int userId) {
		Bill bill = new Bill();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Bill where user_id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("userId", userId);
		if (query.list().size() == 0) return null;
		return query.list();
	}
	
	public void deteleByProId(int proId, int userId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete Cart where pro_id = :proId and user_id = :userId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.setParameter("userId", userId);
		query.executeUpdate();
	}
}
