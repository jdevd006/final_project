package com.jdev006.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Question_Answer;
import com.jdev006.entitties.Review_Product;

@Transactional
@Service
public class Review_Product_service {
	@Autowired
	DAO<Review_Product> Review_Product_DAO;
	
	public List<Review_Product> getAll(){
		return Review_Product_DAO.getAll();
	}
	
	public Review_Product get(int id){
		return Review_Product_DAO.get(id);
	}
	
	public Review_Product add(Review_Product t){
		return Review_Product_DAO.add(t);
	}
	
	public Boolean update(Review_Product t){
		return Review_Product_DAO.update(t);
	}
	
	public Boolean delete(Review_Product t){
		return Review_Product_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Review_Product_DAO.delete(id);
	}
}
