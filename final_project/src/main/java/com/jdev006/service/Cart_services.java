package com.jdev006.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Cart;

@Transactional
@Service
public class Cart_services {
	@Autowired
	private DAO<Cart> Cart_DAO;
	
	public List<Cart> getAll(){
		return Cart_DAO.getAll();
	}
	
	public Cart get(int id){
		return Cart_DAO.get(id);
	}
	
	public Cart add(Cart t){
		return Cart_DAO.add(t);
	}
	
	public Boolean update(Cart t){
		return Cart_DAO.update(t);
	}
	
	public Boolean delete(Cart t){
		return Cart_DAO.delete(t);
	}
	
	public Boolean delete(int id){
		return Cart_DAO.delete(id);
	}
}
