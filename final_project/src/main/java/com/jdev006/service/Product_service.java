package com.jdev006.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Product;

@Transactional
@Service
public class Product_service {
	@Autowired
	DAO<Product> Product_DAO;
	@Autowired
	private SessionFactory sessionFactory;

	public List<Product> getAll() {
		return Product_DAO.getAll();
	}

	public Product get(int id) {
		return Product_DAO.get(id);
	}

	public Product add(Product t) {
		return Product_DAO.add(t);
	}

	public Boolean update(Product t) {
		return Product_DAO.update(t);
	}

	public Boolean delete(Product t) {
		return Product_DAO.delete(t);
	}

	public Boolean delete(int id) {
		return Product_DAO.delete(id);
	}

	public void deteleByProId(int proId) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete Product where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		query.executeUpdate();
	}
	
	public Product getAProduct(int proId) {
		Product product = new Product();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Product where pro_id = :proId";
		Query query = session.createQuery(hql);
		query.setParameter("proId", proId);
		product = (Product) query.list().get(query.list().size()-1);
		return product;
	}
}
