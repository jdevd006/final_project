package com.jdev006.service;

import java.nio.channels.SeekableByteChannel;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jdev006.dao.DAO;
import com.jdev006.entitties.Account_detail;

@Transactional
@Service
public class Account_detail_service {
	@Autowired
	DAO<Account_detail> Account_detail_DAO;
	@Autowired
	private SessionFactory sessionFactory;

	public List<Account_detail> getAll() {
		return Account_detail_DAO.getAll();
	}

	public Account_detail get(int id) {
		return Account_detail_DAO.get(id);
	}

	public Account_detail add(Account_detail t) {
		return Account_detail_DAO.add(t);
	}

	public Boolean update(Account_detail t) {
		return Account_detail_DAO.update(t);
	}

	public Boolean delete(Account_detail t) {
		return Account_detail_DAO.delete(t);
	}

	public Boolean delete(int id) {
		return Account_detail_DAO.delete(id);
	}
	
	public static void main(String[] args) {
		Account_detail_service a = new Account_detail_service();
		a.getByEmail("lamthanh1451999@gamilcom");
	}
	
	public Account_detail getByEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Account_detail a where a.email = :email";
		Query query = session.createQuery(hql);
		query.setParameter("email", email);
		List<Account_detail> listAcc = query.list();
		if (listAcc.size() == 0) return  null;
		return listAcc.get(0);
	}
}
