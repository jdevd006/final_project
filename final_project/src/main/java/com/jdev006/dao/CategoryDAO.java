package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Category;
@Repository
public class CategoryDAO extends DAO<Category> {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Category> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Category").list();
	}

	@Override
	public Category get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Category) session.get(Category.class, id);
	}

	@Override
	public Category add(Category category) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(category);
		return category;
	}

	@Override
	public Boolean update(Category category) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(category);
			return Boolean.TRUE;
		}
		catch(Exception e){
			return Boolean.FALSE;
		}

	}

	@Override
	public Boolean delete(Category category) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != category) 
		{
			try {
				session.delete(category);
				return Boolean.TRUE;
			}
			catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Category category = (Category) session.load(Category.class, id);
		if(null != category)
		{
			session.delete(category);
		}
		return Boolean.FALSE;
	}

}
