package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.BillDetail;

@Repository
public class BillDetailDAO extends DAO<BillDetail>{
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public List<BillDetail> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from BillDetail").list();
	}

	@Override
	public BillDetail get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (BillDetail) session.get(BillDetail.class, id);
	}

	@Override
	public BillDetail add(BillDetail billDetail) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(billDetail);
		return billDetail;
	}

	@Override
	public Boolean update(BillDetail billDetail) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(billDetail);
			return Boolean.TRUE;
		}
		catch(Exception e){
			return Boolean.FALSE;
		}

	}

	@Override
	public Boolean delete(BillDetail billDetail) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != billDetail) 
		{
			try {
				session.delete(billDetail);
				return Boolean.TRUE;
			}
			catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		BillDetail billDetail = (BillDetail) session.load(BillDetail.class, id);
		if(null != billDetail)
		{
			session.delete(billDetail);
		}
		return Boolean.FALSE;
	}

}
