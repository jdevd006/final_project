package com.jdev006.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
public abstract class DAO<T> {
	public abstract List<T> getAll();

	public abstract T get(int id);

	public abstract T add(T t);

	public abstract Boolean update(T t);

	public abstract Boolean delete(T t);

	public abstract Boolean delete(int id);
}
