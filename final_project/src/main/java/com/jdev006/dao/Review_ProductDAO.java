package com.jdev006.dao;

import java.nio.channels.SeekableByteChannel;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Review_Product;
@Repository
public class Review_ProductDAO extends DAO<Review_Product> {
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public List<Review_Product> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Review_Product").list();
	}

	@Override
	public Review_Product get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Review_Product) session.get(Review_Product.class, id);
	}

	@Override
	public Review_Product add(Review_Product review_product) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(review_product);
		return review_product;
	}

	@Override
	public Boolean update(Review_Product review_product) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(review_product);
			return Boolean.TRUE;
		}
		catch(Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Review_Product review_product) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != review_product)
		{
			try {
				session.update(review_product);
				return Boolean.TRUE;
			}
			catch(Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}
	

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Review_Product review_product = (Review_Product) session.load(Review_Product.class, id);
		if(null != review_product) {
			session.delete(review_product);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}
