package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.City;
import com.jdev006.entitties.Viewed;
@Repository
public class ViewedDAO extends DAO<Viewed>{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Viewed> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Viewed").list();
	}

	@Override
	public Viewed get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Viewed) session.get(Viewed.class, id);
	}

	@Override
	public Viewed add(Viewed t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	@Override
	public Boolean update(Viewed t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		}
		catch(Exception e){
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Viewed t) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != t) 
		{
			try {
				session.delete(t);
				return Boolean.TRUE;
			}
			catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		City city = (City) session.load(City.class, id);
		if(null != city)
		{
			session.delete(city);
		}
		return Boolean.FALSE;
	}

}
