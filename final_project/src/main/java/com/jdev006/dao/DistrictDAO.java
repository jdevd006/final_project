package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.District;

@Repository
public class DistrictDAO extends DAO<District> {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<District> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from District").list();
	}

	@Override
	public District get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (District) session.get(District.class, id);
	}

	@Override
	public District add(District t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	@Override
	public Boolean update(District t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		}
		catch(Exception e){
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(District t) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != t) 
		{
			try {
				session.delete(t);
				return Boolean.TRUE;
			}
			catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		District district = (District) session.load(District.class, id);
		if(null != district)
		{
			session.delete(district);
		}
		return Boolean.FALSE;
	}
}
