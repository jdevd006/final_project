package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.City;


@Repository
public class CityDAO extends DAO<City> {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<City> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from City").list();
	}

	@Override
	public City get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (City) session.get(City.class, id);
	}

	@Override
	public City add(City t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	@Override
	public Boolean update(City t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		}
		catch(Exception e){
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(City t) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != t) 
		{
			try {
				session.delete(t);
				return Boolean.TRUE;
			}
			catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		City city = (City) session.load(City.class, id);
		if(null != city)
		{
			session.delete(city);
		}
		return Boolean.FALSE;
	}
}
