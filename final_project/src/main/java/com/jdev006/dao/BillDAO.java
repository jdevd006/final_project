package com.jdev006.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jdev006.entitties.Bill;
@Repository
public class BillDAO extends DAO<Bill> {
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public List<Bill> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Bill").list();
	}

	@Override
	public Bill get(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Bill) session.get(Bill.class, id);
	}

	@Override
	public Bill add(Bill bill) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(bill);
		return bill;
	}

	@Override
	public Boolean update(Bill bill) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(bill);
			return Boolean.TRUE;
		} catch(Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(Bill bill) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != bill)
		{
			try {
				session.delete(bill);
				return Boolean.TRUE;
			}
			catch(Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Bill bill = (Bill) session.load(Bill.class, id);
		if(null != bill)
		{
			session.delete(bill);
		}
		return Boolean.FALSE;
	}

}
